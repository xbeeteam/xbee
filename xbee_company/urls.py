from django.urls import path

from xbee_company import views

app_name = 'xbee_company'

urlpatterns = [
    path('', views.CompaniesView.as_view(), name='companies_list'),
    path('disable/<int:comp_id>', views.DisableCompanyView.as_view(), name='company_disable'),
    path('change/', views.ChangeCompanyView.as_view(), name='company_change')

]
