from django.forms import ModelForm

from xbee_company.models import Company


class CompanyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if not initial.get('white_label', False):
                del self.fields["is_white_label_company"]
            if not (asterisks := initial.get('asterisks', False)):
                self.fields['asterisk'].queryset = asterisks

    class Meta:
        model = Company
        fields = '__all__'
        exclude = ('is_active', 'reseller')
