from django.apps import AppConfig


class XbeeCompanyConfig(AppConfig):
    name = 'xbee_company'
    urls = 'companies'
