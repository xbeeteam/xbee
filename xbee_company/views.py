from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_company.forms import CompanyForm
from xbee_company.models import Company


class CompaniesView(LoginRequiredMixin, BaseClassView):
    """
        This view is accessible only from admin or reseller
        List all companies of the reseller and allows to create a new one
    """

    template_name = 'xbee_company/companies_list.html'
    redirect_url = 'xbee_company:companies_list'

    def dispatch(self, request, *args, **kwargs):
        if not (request.user.has_perm('xbee_account.xbee_perm_reseller_admin') or
                request.user.has_perm('xbee_account.xbee_perm_reseller')):
            raise PermissionDenied
        return super(CompaniesView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        reseller = request.user.cast()
        if request.method == 'POST':
            if 'company_submitted' in request.POST:
                form = CompanyForm(request.POST, request.FILES)
                if form.is_valid():
                    new_company = form.save(commit=False)
                    new_company.reseller = reseller.seller if reseller.seller else reseller
                    new_company.save()
                    messages.success(request, _("Company created!"))
                else:
                    messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        reseller = request.user.cast()
        form_company = CompanyForm(initial={
            'white_label': reseller.is_white_label_reseller,
            'asterisks': reseller.get_asterisks()
        })

        if reseller.selected_company is not None:
            cmp = Company.objects.filter(pk=reseller.selected_company.pk)
        else:
            cmp = reseller.get_companies()

        return render(request, self.template_name, {
            'form_company': form_company,
            'companies': cmp
        })


class DisableCompanyView(LoginRequiredMixin, BaseClassView):
    """
        This view is accessible only from admin or reseller
        View for enable / disable company by id
    """

    redirect_url = 'xbee_company:companies_list'

    def dispatch(self, request, *args, **kwargs):
        if not (request.user.has_perm('xbee_account.xbee_perm_reseller_admin') or
                request.user.has_perm('xbee_account.xbee_perm_reseller')):
            raise PermissionDenied
        return super(DisableCompanyView, self).dispatch(request, *args, **kwargs)

    def get(self, request, comp_id):
        comp = get_object_or_404(Company, id=comp_id)
        comp.is_active = not comp.is_active
        comp.save()

        messages.success(request, _("Company enabled!") if comp.is_active else _("Company disabled!"))
        return redirect(self.redirect_url)


class ChangeCompanyView(LoginRequiredMixin, BaseClassView):
    """
        This view is accessible only from admin or reseller
        Change the selected Company and return to same page
    """

    def dispatch(self, request, *args, **kwargs):
        if not (request.user.has_perm('xbee_account.xbee_perm_reseller_admin') or
                request.user.has_perm('xbee_account.xbee_perm_reseller')):
            raise PermissionDenied
        return super(ChangeCompanyView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        reseller = request.user.cast()
        cmpn = request.GET.get('selected_company', None)
        if cmpn and cmpn.isnumeric():
            reseller.selected_company = Company.objects.get(
                Q(reseller=reseller) | Q(reseller=reseller.seller),
                pk=cmpn
            )
        else:
            reseller.selected_company = None
        reseller.save()

        return redirect(request.GET.get('next'))
