from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Company(models.Model):
    id = models.AutoField(primary_key=True, db_column='comp_id')
    name = models.CharField(max_length=120, db_column='comp_name')
    max_users = models.IntegerField(default=0, null=False, db_column='comp_max_users')
    asterisk = models.ForeignKey('xbee_asterisk.Asterisk', related_name='companies', on_delete=models.PROTECT,
                                 db_column='comp_asterisk_id')
    is_white_label_company = models.BooleanField(default=False, null=False, db_column='comp_is_white_label')
    logo = models.ImageField(default=None, upload_to="companies/logo/", blank=True, null=True, db_column='comp_logo')
    is_active = models.BooleanField(default=True, blank=False, null=False, db_column='comp_enabled')
    reseller = models.ForeignKey('xbee_account.Reseller', null=False, on_delete=models.CASCADE,
                                 related_name='reseller_companies', db_column='comp_reseller')
    modules = models.ManyToManyField('xbee_modules.Module', related_name="company_modules")

    @staticmethod
    @receiver(post_save, sender='xbee_company.Company')
    def superuser_post_save(sender, instance, created, **kwargs):
        if created:
            from xbee_recordings.models import MusicOnHoldingCategory
            mohc = MusicOnHoldingCategory()
            mohc.company = instance
            mohc.name = 'Default'
            mohc.save()

    def __str__(self):
        return f"{self.name} [{self.max_users=}]"

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'
        db_table = 'xbee_company'
