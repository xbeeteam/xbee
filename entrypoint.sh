#!/bin/bash

cd /app
pip3 install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py runserver "0.0.0.0:8000" --settings=xbee.settings.docker