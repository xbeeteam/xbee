from django.urls import path

from xbee_recordings import views

app_name = 'xbee_recordings'

urlpatterns = [
    path('system-recording', views.SystemRecordingsView.as_view(), name='system_recording'),
    path('music-on-holding', views.MusicOnHoldingsView.as_view(), name='music_on_holding'),
    path('download/<int:id_recording>', views.DownloadRecordingView.as_view(), name='download_recording'),
    path('delete/<int:id_recording>', views.DeleteRecordingView.as_view(), name='delete_recording'),

]
