
function downlaodRecording(obj) {
    let url = $(obj).attr('url');
    $(obj).attr('disabled', 'disabled');

    $.ajax({url: url})
        .done(function(resp) {
            let tagP       = document.createElement('p');
            $(tagP).attr('class', 'text-muted mt-2 mr-4');
            let sound      = document.createElement('audio');
            sound.controls = 'controls';
            sound.src      = 'data:audio/x-wav;base64,' + resp;
            $(tagP).append($(sound));
            $(tagP).insertAfter($(obj));
        });
}