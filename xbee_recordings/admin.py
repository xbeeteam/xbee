from django.contrib import admin

from xbee_recordings.models import SystemRecording, MusicOnHolding, MusicOnHoldingCategory, RecordingFile


admin.site.register(SystemRecording)
admin.site.register(MusicOnHolding)
admin.site.register(MusicOnHoldingCategory)
admin.site.register(RecordingFile)
