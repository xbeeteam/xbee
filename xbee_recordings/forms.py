from django.forms import ModelForm
from django import forms

from xbee_recordings.models import MusicOnHolding, SystemRecording, MusicOnHoldingCategory


class MusicOnHoldingForm(ModelForm):
    source_file = forms.FileField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if categs := initial.get('categories', False):
                self.fields['category'].queryset = categs

            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps

    class Meta:
        model = MusicOnHolding
        fields = '__all__'


class SystemRecordingForm(ModelForm):
    source_file = forms.FileField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields["company"].queryset = comps

    class Meta:
        model = SystemRecording
        fields = '__all__'


class MusicOnHoldingCategoryForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields["company"].queryset = comps

    class Meta:
        model = MusicOnHoldingCategory
        fields = '__all__'
