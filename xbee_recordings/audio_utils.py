import base64
import os

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.utils.crypto import get_random_string
from pydub import AudioSegment

from xbee_recordings.models import RecordingFile


def format_conversion(recording, source_path):
    """
    Convert the source_path file, in wav mono 16 bit 8000 hz [= 8 khz]

    :param recording: instance of xbee_recordings.models.Recording
    :param source_path: path of source file
    :return: instance of File, result converted file
    """
    if not os.path.exists(f"{settings.MEDIA_ROOT}/temp"):
        os.mkdir(f"{settings.MEDIA_ROOT}/temp")

    path_src, filename = os.path.split(source_path)
    file_name, file_extension = os.path.splitext(filename)
    tmp_file_name = os.path.join(settings.MEDIA_ROOT,
                                 f"temp/{get_random_string(length=5)}_{file_name}.wav")
    if 'mp3' in file_extension.lower():
        sound = AudioSegment.from_mp3(source_path)
    else:
        sound = AudioSegment.from_wav(source_path)
    sound = sound.set_frame_rate(8000)
    sound = sound.set_channels(1)  # mono
    sound = sound.set_sample_width(2)

    if recording.convert_to == 'wav':  # always True for now
        converted_file = sound.export(tmp_file_name, format='wav')
    # todo add other format when required

    return converted_file


def save_recording_file(recording, converted_path):
    """
    Save into db the audio files

    :param recording: instance of xbee_recordings.models.Recording
    :param converted_path: path of converted file
    :return:
    """
    with open(converted_path, "rb") as conv_f:
        rc = RecordingFile(recording=recording)
        rc.converted_file = conv_f.read()
        rc.save()

        conv_f.close()


def clean_temp_files(source_path, converted_path):
    """
    Delete temp files

    :param source_path: source file path
    :param converted_path: converted file path
    :return:
    """
    try:
        os.remove(source_path)
    except:
        pass
    try:
        os.remove(converted_path.name)
    except:
        pass


def get_source_path_file(request):
    """
    Store in a temp directory the source audio file

    :param request: Django HttpRequest object
    :return: path of the source audio file
    """
    data = request.FILES['source_file']
    path = default_storage.save(f'temp/{data.name}', ContentFile(data.read()))
    src_file_path = os.path.join(settings.MEDIA_ROOT, path)
    return src_file_path


def from_memory_to_base64(memoryv):
    """
    Convert bytes in base64 encoding

    :param memoryv: file bytes
    :return: base64 encoding of the file or ''
    """
    try:
        return base64.b64encode(bytes(memoryv)).decode('ascii')
    except Exception:
        return ''
