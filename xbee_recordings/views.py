from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import ProtectedError
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_recordings.forms import MusicOnHoldingForm, MusicOnHoldingCategoryForm, SystemRecordingForm
from xbee_recordings.models import MusicOnHolding, MusicOnHoldingCategory, SystemRecording, RecordingFile, Recording
from xbee_recordings.audio_utils import save_recording_file, format_conversion, clean_temp_files, \
    get_source_path_file, from_memory_to_base64


class SystemRecordingsView(LoginRequiredMixin, BaseClassView):
    """
        List and create the system recordings
    """

    template_name = 'xbee_recordings/system_recordings.html'
    redirect_url = 'xbee_recordings:system_recording'

    def post(self, request):
        if 'sysr_submitted' in request.POST:
            form = SystemRecordingForm(request.POST, request.FILES)
            if form.is_valid():
                try:
                    # atomic transaction, if something in the conversion was wrong the MusicOnHolding was deleted
                    with transaction.atomic():
                        src_file_path = get_source_path_file(request)

                        new_sysr = form.save(commit=False)
                        new_sysr.save()

                        # format conversion, store into db
                        converted_file = format_conversion(new_sysr, src_file_path)
                        save_recording_file(new_sysr, converted_file.name)

                        messages.success(request, _("System recording created!"))
                except Exception:
                    messages.error(request, _("Somethings goes wrong!"))
                finally:
                    # cleaning
                    clean_temp_files(src_file_path, converted_file)

            else:
                messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        sysrec = SystemRecording.objects.filter(company__in=comps)
        form_sysr = SystemRecordingForm(initial={'companies': comps})

        return render(request, self.template_name, {
            'form_sysr': form_sysr,
            'sysrec': sysrec
        })


class MusicOnHoldingsView(LoginRequiredMixin, BaseClassView):
    """
        List and create the music on holding and categories
    """

    template_name = 'xbee_recordings/music_on_holding.html'
    redirect_url = 'xbee_recordings:music_on_holding'

    def post(self, request):
        if 'moh_submitted' in request.POST:
            form = MusicOnHoldingForm(request.POST, request.FILES)
            if form.is_valid():
                try:
                    # atomic transaction, if something in the conversion was wrong the MusicOnHolding was deleted
                    with transaction.atomic():
                        src_file_path = get_source_path_file(request)

                        new_moh = form.save(commit=False)
                        new_moh.save()

                        # format conversion, store into db
                        converted_file = format_conversion(new_moh, src_file_path)
                        save_recording_file(new_moh, converted_file.name)

                        messages.success(request, _("Music on holding created!"))
                except Exception:
                    messages.error(request, _("Somethings goes wrong!"))
                finally:
                    # cleaning
                    clean_temp_files(src_file_path, converted_file)
            else:
                messages.error(request, _("Somethings goes wrong!"))

        if 'moh_category_submitted' in request.POST:
            form = MusicOnHoldingCategoryForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, _("Category created!"))
            else:
                messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        categories = MusicOnHoldingCategory.objects.filter(company__in=comps)
        music = MusicOnHolding.objects.filter(category__in=categories)
        form_moh = MusicOnHoldingForm(initial={'categories': categories, 'companies': comps})
        form_moh_category = MusicOnHoldingCategoryForm(initial={'companies': comps})

        return render(request, self.template_name, {
            'form_moh': form_moh,
            'form_moh_category': form_moh_category,
            'music': music, 'categories': categories
        })


class DownloadRecordingView(LoginRequiredMixin, BaseClassView):
    """
        Selects the file stored into the db and return base64 encode of them
    """

    def get(self, request, id_recording):
        base64_file = from_memory_to_base64(
            RecordingFile.objects.get(recording__pk=id_recording).converted_file
        )
        return HttpResponse(base64_file, content_type='text/plain', status=200)


class DeleteRecordingView(LoginRequiredMixin, BaseClassView):
    """
        delete recordings by id
    """

    def get(self, request, id_recording):
        recs = Recording.objects.get(pk=id_recording)
        if recs.real_type.model == 'musiconholding':
            redirect_to = 'music_on_holding'
        elif recs.real_type.model == 'systemrecording':
            redirect_to = 'system_recording'

        try:
            recs.delete()
            messages.success(request, _("Deleted!"))
        except ProtectedError as e:
            context = {'queryset': e.protected_objects}
            html_content = render_to_string('exception-message-template.html', context)
            messages.warning(
                request,
                _("This file is associated with another module, please remove the association and try again.")
                + html_content
            )

        return redirect(f'xbee_recordings:{redirect_to}')
