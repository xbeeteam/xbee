import sys
from django.apps import AppConfig


class XbeeRecordingsConfig(AppConfig):
    name = 'xbee_recordings'
    urls = 'recordings'
    verbose_name = 'Recordings'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='SystemRecording')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_recordings.models import SystemRecording

                Module.objects.create(
                    name='SystemRecording',
                    extension=None,
                    model_object=ContentType.objects.get_for_model(SystemRecording)
                ).save()

            try:
                Module.objects.get(name='MusicOnHolding')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_recordings.models import MusicOnHolding

                Module.objects.create(
                    name='MusicOnHolding',
                    extension=None,
                    model_object=ContentType.objects.get_for_model(MusicOnHolding)
                ).save()
