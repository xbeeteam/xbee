from django.contrib.contenttypes.models import ContentType
from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as _t


class RecordingFile(models.Model):
    converted_file = models.BinaryField(db_column='recfl_converted_file')
    recording = models.ForeignKey('Recording', null=False, on_delete=models.CASCADE, related_name="recording_file",
                                  db_column='recfl_rec_id')

    def __str__(self):
        return self.recording.company.name

    class Meta:
        verbose_name = 'RecordingFile'
        verbose_name_plural = 'RecordingFiles'
        db_table = 'xbee_recordingfile'


class Recording(models.Model):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_recordings", db_column='rec_comp_id')
    real_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, editable=False)

    FORMAT_CHOICES = (
        ('wav', _('WAV')),
    )
    convert_to = models.CharField(default='wav', choices=FORMAT_CHOICES, max_length=5, db_column='rec_convert_to')

    def __str__(self):
        return self.company.name

    def get_real_type(self):
        return ContentType.objects.get_for_model(type(self))

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.real_type = self.get_real_type()
        super(Recording, self).save(*args, **kwargs)

    def cast(self):
        return self.real_type.get_object_for_this_type(pk=self.pk)

    class Meta:
        verbose_name = 'Recording'
        verbose_name_plural = 'Recordings'
        db_table = 'xbee_recording'


class SystemRecording(Recording):
    name = models.CharField(max_length=30, null=False, blank=False, db_column='srec_name')
    description = models.TextField(db_column='srec_description')

    def __str__(self):
        return f"{self.company.name} - {self.name}"

    class Meta:
        verbose_name = 'SystemRecording'
        verbose_name_plural = 'SystemRecordings'
        db_table = 'xbee_systemrecording'


class MusicOnHolding(Recording):
    is_random_play = models.BooleanField(default=False, db_column='moh_is_random_play')
    category = models.ForeignKey('MusicOnHoldingCategory', null=False, on_delete=models.CASCADE,
                                 related_name="music_category", db_column='moh_mohcategory_id')

    def __str__(self):
        return self.company.name

    class Meta:
        verbose_name = 'MusicOnHolding'
        verbose_name_plural = 'MusicOnHoldings'
        db_table = 'xbee_musiconholding'


class MusicOnHoldingCategory(models.Model):
    name = models.CharField(max_length=30, null=False, blank=False, db_column='mohc_name')
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_music_categories", db_column='mohc_comp_id')

    def __str__(self):
        return _t(f'{self.name} - [Company: {self.company.name}]')

    class Meta:
        verbose_name = 'MusicOnHoldingCategory'
        verbose_name_plural = 'MusicOnHoldingCategories'
        db_table = 'xbee_musiconholdingcategory'
