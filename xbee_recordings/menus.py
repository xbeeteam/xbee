from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("System Recording"), reverse('xbee_recordings:system_recording'), weight=200,
                               icon="iconsminds-sound"))
Menu.add_item("main", MenuItem(_("Music on Holding"), reverse('xbee_recordings:music_on_holding'), weight=210,
                               icon="simple-icon-music-tone-alt"))
