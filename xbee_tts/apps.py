import sys

from django.apps import AppConfig


class XbeeTtsConfig(AppConfig):
    name = 'xbee_tts'
    urls = 'tts'
    context = 'tts'
    extension = 's'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='TTS')
            except Module.DoesNotExist:
                Module.objects.create(
                    name='TTS',
                    extension=self.extension,
                    context=self.context,
                ).save()

