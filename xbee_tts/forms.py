from django import forms
from django.utils.translation import ugettext_lazy as _


class TTSForm(forms.Form):
    text = forms.CharField()

    LANG_CHOICES = (
        ('it', _('Italian')),
        ('fr', _('French')),
        ('es', _('Spanish')),
        ('en', _('English')),
        ('ru', _('Russian')),
    )
    language = forms.ChoiceField(choices=LANG_CHOICES)
