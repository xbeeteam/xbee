from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("TTS"), reverse('xbee_tts:tts_create'), weight=1000,
                               icon="iconsminds-voice"))
