from django.urls import path

from xbee_tts import views

app_name = 'xbee_tts'

urlpatterns = [
    path('', views.TTSCreateView.as_view(), name='tts_create'),

]
