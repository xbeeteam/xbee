import os
import datetime as dt

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.utils.crypto import get_random_string
from gtts import gTTS

from utils.base_class_view import BaseClassView
from xbee_tts.forms import TTSForm


class TTSCreateView(LoginRequiredMixin, BaseClassView):
    """
        Allows to create new tts recording
    """

    redirect_url = 'xbee_tts:tts_create'
    template_name = 'xbee_tts/tts_create.html'
    url_recording = ''

    def post(self, request):
        form = TTSForm(request.POST)
        if form.is_valid():
            tts = gTTS(form.cleaned_data.get('text'), lang=form.cleaned_data.get('language'))

            if not os.path.exists(f"{settings.MEDIA_ROOT}/temp/tts"):
                os.mkdir(f"{settings.MEDIA_ROOT}/temp/tts")
            else:
                # clean directory
                now = dt.datetime.now()
                ago = now - dt.timedelta(hours=2)
                for root, dirs, files in os.walk(f"{settings.MEDIA_ROOT}/temp/tts"):
                    for fname in files:
                        path = os.path.join(root, fname)
                        st = os.stat(path)
                        mtime = dt.datetime.fromtimestamp(st.st_mtime)
                        if mtime < ago:
                            os.remove(path)

            rel_path_mp3 = f'temp/tts/{get_random_string(length=5)}recording.mp3'
            file_path = os.path.join(settings.MEDIA_ROOT, rel_path_mp3)
            tts.save(file_path)
            self.url_recording = f'{settings.MEDIA_URL}{rel_path_mp3}'

        response = redirect(self.redirect_url)
        response['Location'] += f'?tts={self.url_recording}'
        return response

    def get(self, request):
        form_tts = TTSForm()
        return render(request, self.template_name, {'form_tts': form_tts})
