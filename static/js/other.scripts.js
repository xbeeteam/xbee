$(function () {
    $.ajaxSetup({ beforeSend: function(xhr, settings) { if (!csrfSafeMethod(settings.type) && !this.crossDomain) { xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken')); } } });

    /* start autoselect the first option of the select with only one option */
    let allSelect = $('form.custom-form select');
    for (let i = 0; i < allSelect.length; i++) {
        if ($(allSelect[i]).children('option').length == 2 && $(allSelect[i]).children('option').first().val() == '') {
            $($(allSelect[i]).children('option').get(1)).attr('selected', true);
        }
    }
    /* end autoselect */

    /* start custom dropzone */
    $(".dropzone").change(function() {
        readFile(this);
    });
    $('.dropzone-wrapper').on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('dragover');
    });
    $('.dropzone-wrapper').on('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('dragover');
    });
    $('.remove-preview').on('click', function() {
        var boxZone = $(this).parents('.preview-zone').find('.box-body');
        var previewZone = $(this).parents('.preview-zone');
        var dropzone = $(this).parents('.form-group').find('.dropzone');
        boxZone.empty();
        previewZone.addClass('hidden');
        reset(dropzone);
    });
    /* end custom dropzone */

    /* start module ajax call for modules list */
    attachListenerToSelectModule();
    /* end module ajax */

});

function attachListenerToSelectModule() {
    $('form.custom-form select[my-id=module_select_ajax]').on('change', function () {
        let selectObj = this;
        $.post($(selectObj).attr('url'), {'module_pk': $(selectObj).val()})
            .done(function( data ) {
                if (/^/.test(data)) {
                    $(`form.custom-form select[my-id=module_select_ajax_sub][id=${$(selectObj).attr('id').replace('module', 'sub_module')}]`).empty().append(data);
                }
            })
            .fail(function(error) {
                $(`form.custom-form select[my-id=module_select_ajax_sub][id=${$(selectObj).attr('id').replace('module', 'sub_module')}]`).empty();
            });
    });
}

function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var validFileType = ".jpg , .jpeg , .png , .gif , .bmp";
            var extension = input.files[0].name.split('.').pop().toLowerCase();
            var htmlPreview =
                (validFileType.toLowerCase().indexOf(extension) > 0 ?'<img width="150" src="' + e.target.result + '" />' : '') +
                '<p>' + input.files[0].name + '</p>';
            var wrapperZone = $(input).parent();
            var previewZone = $(input).parent().parent().find('.preview-zone');
            var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

            wrapperZone.removeClass('dragover');
            previewZone.removeClass('hidden');
            boxZone.empty();
            boxZone.append(htmlPreview);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function reset(e) {
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}

function onChangeSelectedCompany(obj) {
    $(obj).closest("form").submit();
}

function getCookie(cookiename) {
    var cookiestring = RegExp(""+cookiename+"[^;]+").exec(document.cookie);
    return decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");
}

function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}