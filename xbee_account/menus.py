from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
dashboard_children = (
    MenuItem("Default", reverse('xbee_account:dashboard'), weight=10, icon="simple-icon-rocket"),
    MenuItem("Analytics", '#', weight=20, icon="simple-icon-pie-chart"),
    MenuItem("Ecommerce", '#', weight=30, icon="simple-icon-basket-loaded", children=(
        MenuItem("Order", '#', weight=10, icon="simple-icon-rocket"),
        MenuItem("Product", '#', weight=20, icon="simple-icon-rocket"),
    ), identifier='ecommercesubmenu'),
)

Menu.add_item(
    "main",
    MenuItem(
        _("Dashboard"),
        '#',
        weight=10,
        children=dashboard_children, icon="iconsminds-shop-4", identifier='dashboard')
)
Menu.add_item(
    "main",
    MenuItem(
        _("Users"),
        reverse('xbee_account:users_list'),
        weight=30,
        icon="iconsminds-male-female"
    )
)

# dropdown account menu
Menu.add_item(
    "dropdown_account_menu",
    MenuItem(
        _("Account"),
        reverse('xbee_account:account', kwargs={'user_id': 0}),
        weight=10
    )
)

Menu.add_item(
    "dropdown_account_menu",
    MenuItem(
        _("DB admin"),
        reverse("admin:index"),
        weight=89,
        check=lambda request: request.user.is_superuser)
)
Menu.add_item(
    "dropdown_account_menu",
    MenuItem(_("Sign out"), reverse('xbee_account:logout'), weight=90)
)
