from django.urls import path

from xbee_account import views

app_name = 'xbee_account'

urlpatterns = [
    path('', views.DashboardView.as_view(), name='dashboard'),
    path('users-list/', views.users_list, name='users_list'),
    path('user/detail/<int:user_id>', views.user_detail, name='account'),
    path('user/disable/<int:user_id>', views.user_disable, name='user_disable'),

    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),

]
