from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class XbeeUser(AbstractUser):
    profile_image = models.ImageField(upload_to="users/profile/", blank=True, null=True, db_column='usr_image')
    vat_number = models.CharField(max_length=16, db_column='usr_vat_number')
    address = models.TextField(blank=True, db_column='usr_address')
    real_type = models.ForeignKey(ContentType, null=True, on_delete=models.CASCADE, editable=False)

    @staticmethod
    @receiver(post_save, sender='xbee_account.XbeeUser')
    def superuser_post_save(sender, instance, created, **kwargs):
        if instance.is_superuser and created:
            reseller = Reseller(xbeeuser_ptr_id=instance.pk)
            reseller.__dict__.update(instance.__dict__)
            reseller.is_white_label_reseller = True
            reseller.real_type = ContentType.objects.get_for_model(Reseller)
            reseller.save()

    def get_real_type(self):
        return ContentType.objects.get_for_model(type(self))

    def get_absolute_url(self):
        return reverse("xbee_account:account", kwargs={"user_id": self.pk})

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.real_type = self.get_real_type()
        super(XbeeUser, self).save(*args, **kwargs)

    def cast(self):
        return self.real_type.get_object_for_this_type(pk=self.pk)

    class Meta:
        verbose_name = 'XbeeUser'
        verbose_name_plural = 'XbeeUsers'


class Customer(XbeeUser):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE, related_name="customers",
                                db_column='usr_comp_id')

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'
        permissions = (
            ("xbee_perm_client", _("Client")),
        )


class Reseller(XbeeUser):
    seller = models.ForeignKey('Reseller', default=None, null=True, blank=True, on_delete=models.CASCADE,
                               db_column='usr_reseller')
    is_white_label_reseller = models.BooleanField(default=False, null=False, db_column='usr_is_white_label_reseller')
    selected_company = models.ForeignKey('xbee_company.Company', default=None, null=True, on_delete=models.SET_NULL,
                                         related_name="+", db_column='usr_sel_comp_id')

    def get_asterisks(self):
        return self.seller.reseller_asterisks.all() if self.seller else self.reseller_asterisks.all()

    def get_companies(self):
        return self.seller.reseller_companies.all() if self.seller else self.reseller_companies.all()

    class Meta:
        verbose_name = 'Reseller'
        verbose_name_plural = 'Resellers'
        permissions = (
            ("xbee_perm_reseller_admin", _("Reseller admin")),
            ("xbee_perm_reseller", _("Reseller")),
        )
