/* global var */
let listOfIp = [];
let cpuGraphList = [];
let ramGraphList = [];
let netGraphList = [];

$(function () {
    /* obtaining ip list from html */
    server = $('#cpu-stats');
    if ($(server).length > 0) {
        ipp = $(server).find('div[ip]');
        for (const ip of ipp) {
            listOfIp.push($(ip).attr('ip'));
        }
    }

    /* CPU */
    setInterval(function () {
        /* cicle over the ip list and get stats every 3 sec */
        for (const ip of listOfIp) {
            $.get(`http://${ip}:19999/api/v1/data?chart=system.cpu&after=-1`)
                .done(function (data) {
                    let allData = data['data'][0];
                    let perc = 0;

                    for (let j = 1; j < allData.length; j++) {perc += allData[j]; }

                    let roundPerc = Math.round(perc);
                    $(`#cpu-stats div[ip='${ip}'] span.perc`).html(roundPerc);
                    updateChart(ip, roundPerc, cpuGraphList, '#cpu-stats');
                });
        }
    }, 3000);

    /* RAM */
    for (const ip of listOfIp) {
        $.get(`http://${ip}:19999/api/v1/data?chart=system.ram&after=-1`)
            .done(function (data) {
                let allData = data['data'][0];
                let mem = 0;

                for (let j = 1; j < allData.length; j++) {mem += allData[j]; }

                let roundMem = Math.round(mem / 1024 * 100) / 100;
                $(`#ram-stats div[ip='${ip}'] span.tot-mem`).html(roundMem);
                ramGraphList.push({ip: ip, mem: mem});
            });
    }
    setInterval(function () {
        /* cicle over the ip list and get stats every 3 sec */
        for (const ip of listOfIp) {
            $.get(`http://${ip}:19999/api/v1/data?chart=mem.available&after=-1`)
                .done(function (data) {
                    let allData = data['data'][0];
                    let totRam = ramGraphList[searchPosInArray(ramGraphList, ip)].mem;
                    let usedRam = Math.round(totRam - allData[1]);

                    let roundPerc = Math.round(100 * usedRam / totRam);
                    $(`#ram-stats div[ip='${ip}'] span.perc`).html(roundPerc);
                    updateChart(ip, roundPerc, ramGraphList, '#ram-stats');
                });
        }
    }, 3000);

    /* NET */
    setInterval(function () {
        /* cicle over the ip list and get stats every 3 sec */
        for (const ip of listOfIp) {
            $.get(`http://${ip}:19999/api/v1/data?chart=system.net&after=-1`)
                .done(function (data) {
                    let allData = data['data'][0];

                    let down = Math.round(allData[1] * 100) / 100;
                    let up = Math.round(allData[2] * 100) / 100;

                    $(`#net-stats div[ip='${ip}'] span.down`).html(down);
                    $(`#net-stats div[ip='${ip}'] span.up`).html(up);
                    updateNetChart(ip, up, down);
                });
        }
    }, 3000);
});

function updateChart(ip, perc, chartList, selector) {
    let pos = searchPosInArray(chartList, ip);
    if (pos >= 0 && chartList[pos].chart) {
        let achart = chartList[pos].chart;
        achart.config.data.datasets[0].data.push({y: perc, x: new Date()});
        achart.update();
    } else {
        let ctx = $(`${selector} div[ip='${ip}'] canvas`);
        let achart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{data: [{y: perc, x: new Date()}, ], borderColor: 'blue', backgroundColor: 'red', fill: false,}]
            },
            options: {
                legend: {display: false,},
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {stepSize: 20, suggestedMin: 0, suggestedMax: 100, beginAtZero: true,}
                    }],
                    xAxes: [{type: 'time', time: {unit: 'minute', }}]
                }
            }
        });
        if (pos >= 0) {
            chartList[pos].chart = achart;
        } else {
            chartList.push({ip: ip, chart: achart});
        }
    }
}

function searchPosInArray(arrayToSearch, ip) {
    return arrayToSearch.map(function(e) { return e.ip; }).indexOf(ip);
}

function updateNetChart(ip, up, down) {
    let pos = searchPosInArray(netGraphList, ip);
    let adate = new Date();
    if (pos >= 0 && netGraphList[pos].chart) {
        let achart = netGraphList[pos].chart;
        achart.config.data.datasets[0].data.push({y: up, x: adate});
        achart.config.data.datasets[1].data.push({y: down, x: adate});
        achart.update();
    } else {
        let ctx = $(`#net-stats div[ip='${ip}'] canvas`);
        let achart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [
                    {data: [{y: up, x: adate}, ], borderColor: 'blue', backgroundColor: 'blue', fill: false,},
                    {data: [{y: down, x: adate}, ], borderColor: 'red', backgroundColor: 'red', fill: false,},
                ]
            },
            options: {
                legend: {display: false,},
                scales: {
                    xAxes: [{type: 'time', time: {unit: 'minute', }}]
                }
            }
        });
        netGraphList.push({ip: ip, chart: achart});
    }
}