from django.contrib import messages
from django.contrib.auth import (
    logout as _logout, login as _login, authenticate,
    get_user_model, update_session_auth_hash)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Permission
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.views import View

from xbee_account.forms import ResellerForm, CustomerForm, EditUserForm
from xbee_account.models import Customer, Reseller, XbeeUser
from xbee_asterisk.models import Asterisk
from xbee_company.models import Company


class DashboardView(LoginRequiredMixin, View):
    def get(self, request):
        sub_user_type = request.user.cast()  # can be an instance of Reseller or Customer
        if request.user.has_perm('xbee_account.xbee_perm_reseller_admin'):
            asterisks = Asterisk.objects.all()
        elif request.user.has_perm('xbee_account.xbee_perm_reseller'):
            asterisks = sub_user_type.get_asterisks()

        return render(request, 'xbee_account/dashboard.html', {'asterisks': asterisks})


@login_required
def users_list(request):
    """
    This view show to a reseller all the user that he have created and can create new users (resellers or clients)

    :param request: Django HttpRequest object
    :return: Django HttpResponse object
    """
    if request.method == 'POST':
        if 'client_submitted' in request.POST:
            form = CustomerForm(request.POST)
            if form.is_valid():
                permission = Permission.objects.get(codename='xbee_perm_client')

                new_client = form.save(commit=False)
                if new_client.company.max_users > len(new_client.company.customers.all()):
                    new_client.save()
                    new_client.user_permissions.add(permission)
                    messages.success(request, _("Client created!"))
                else:
                    messages.error(request, _("Too many users for this company."))
            else:
                messages.error(request, _("Somethings goes wrong!"))

        if 'reseller_submitted' in request.POST:
            form = ResellerForm(request.POST)
            if form.is_valid():
                permission = Permission.objects.get(codename='xbee_perm_reseller')

                new_reseller = form.save(commit=False)
                new_reseller.save()
                new_reseller.user_permissions.add(permission)
                messages.success(request, _("Reseller created!"))
            else:
                messages.error(request, _("Somethings goes wrong!"))

    # initialization of the forms and data to show by permission
    sub_user_type = request.user.cast()  # can be an instance of Reseller or Customer
    if request.user.has_perm('xbee_account.xbee_perm_reseller_admin'):
        if sub_user_type.selected_company is None:
            companies = Company.objects.all()
        else:
            companies = Company.objects.filter(pk=sub_user_type.selected_company.pk)

        resellers = Reseller.objects.all()
        form_reseller = ResellerForm(initial={'white_label': sub_user_type.is_white_label_reseller})
    elif request.user.has_perm('xbee_account.xbee_perm_reseller'):
        if sub_user_type.selected_company is None:
            companies = Company.objects.filter(reseller=sub_user_type)
        else:
            companies = Company.objects.filter(pk=sub_user_type.selected_company.pk)

        resellers = Reseller.objects.filter(seller=sub_user_type.seller if sub_user_type.seller else sub_user_type)
        form_reseller = ResellerForm(initial={'white_label': sub_user_type.is_white_label_reseller})
    elif request.user.has_perm('xbee_account.xbee_perm_client'):
        companies = Company.objects.filter(pk=sub_user_type.company.pk)

        resellers = []
        form_reseller = ResellerForm()

    form_client = CustomerForm(initial={'companies': companies})
    clients = Customer.objects.filter(company__in=companies)
    return render(request, 'xbee_account/users_list.html', {
        'form_client': form_client,
        'form_reseller': form_reseller,
        'clients': clients,
        'resellers': resellers
    })


@login_required
def user_disable(request, user_id):
    """
    View for enable / disable user by id

    :param request: Django HttpRequest object
    :param user_id: id of the user to enable / disable
    :return: Django HttpResponse object
    """
    usr = get_object_or_404(get_user_model(), id=user_id)
    usr.is_active = not usr.is_active
    usr.save()

    messages.success(request, _("User enabled!") if usr.is_active else _("User disabled!"))
    return redirect('xbee_account:users_list')


def login(request):
    """
    This view allows any unauthenticated user
    to login using username and password of any active User.
    Redirects to dashboard page.

    :param request: Django HttpRequest object
    :return: Django HttpResponse object
    """
    if request.user.is_authenticated:
        return redirect('xbee_account:dashboard')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            next = request.POST.get('next')

            if username and password:
                user = authenticate(request, username=username, password=password)
                if user is not None:
                    if user.is_active:
                        _login(request, user)
                        if next:
                            return redirect(next)
                        else:
                            return redirect('xbee_account:dashboard')
                    else:
                        messages.error(request, _("This account is not active!"))
                else:
                    messages.error(request, _("Your username and password does not match!"))
                    return redirect('xbee_account:login')
            else:
                messages.warning(request, _("Missing required fields!"))
                return redirect('xbee_account:dashboard')
        else:
            return render(request, 'login.html')


@login_required
def logout(request):
    """
    This view logs the user out and redirects to index page.

    :param request: Django HttpRequest object
    :return: Django HttpResponse object
    """
    _logout(request)
    messages.success(request, _("You have logged out successfully!"))
    return redirect('xbee_account:login')


@login_required
def user_detail(request, user_id=0):
    """
    This view show the user details and allow to edit some information.

    :param request: Django HttpRequest object
    :param user_id: id of the user to show
    :return: Django HttpResponse object
    """
    usr = request.user if user_id == 0 else XbeeUser.objects.filter(pk=user_id)

    if request.method == 'POST':
        if 'edit_profile' in request.POST:
            # TODO
            pass

        if 'change_password' in request.POST:
            form = PasswordChangeForm(usr, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)
                messages.success(request, _("Password updated!"))
            else:
                messages.error(request, _("Somethings goes wrong!"))

    form_psw_change = PasswordChangeForm(usr)
    form_edit = EditUserForm()
    return render(request, 'xbee_account/user_detail.html', {
        'form_psw_change': form_psw_change,
        'form_edit': form_edit
    })
