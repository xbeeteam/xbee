from django import forms
from django.forms import ModelForm

from django.utils.translation import ugettext_lazy as _

from xbee_account.models import Reseller, Customer


class ResellerForm(ModelForm):
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if not initial.get('white_label', False):
                del self.fields["is_white_label_reseller"]

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()

        return user

    class Meta:
        model = Reseller
        fields = ("username", "password", "email", "first_name", "last_name", "vat_number", "is_white_label_reseller")


class CustomerForm(ModelForm):
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            self.fields["company"].queryset = initial.get('companies')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()

        return user

    class Meta:
        model = Customer
        fields = ("username", "password", "email", "first_name", "last_name", "vat_number", "company")


class EditUserForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            # TODO
            pass

    class Meta:
        model = Customer
        fields = ('first_name', 'last_name', 'profile_image', 'vat_number', 'address')
