from django.apps import AppConfig


class XbeeAccountConfig(AppConfig):
    name = 'xbee_account'
    urls = ''
