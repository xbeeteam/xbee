from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from xbee_account.models import Customer, Reseller, XbeeUser


admin.site.register(XbeeUser, UserAdmin)
admin.site.register(Customer)
admin.site.register(Reseller)
