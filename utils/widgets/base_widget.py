from abc import abstractmethod, ABC

from django.template import loader


class BaseWidget(ABC):

    def __init__(self, user):
        self.context = {}
        self.html_file = ''
        self.user = user

    @classmethod
    def instantiate(cls, user):
        return cls(user)

    @abstractmethod
    def define_context(self):
        pass

    @abstractmethod
    def define_html_path_file(self):
        pass

    def get_companies_by_user(self):
        from xbee_company.models import Company

        sub_user_type = self.user.cast()
        if self.user.has_perm('xbee_account.xbee_perm_reseller_admin'):
            companies = Company.objects.all()
        elif self.user.has_perm('xbee_account.xbee_perm_reseller'):
            companies = sub_user_type.get_companies()
        elif self.user.has_perm('xbee_account.xbee_perm_client'):
            companies = [sub_user_type.company]

        return companies

    def render(self):
        self.define_html_path_file()
        self.define_context()
        tpl = loader.get_template(self.html_file)
        return tpl.render(self.context)
