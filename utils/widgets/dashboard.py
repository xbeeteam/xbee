from utils.widgets.base_widget import BaseWidget


class Dashboard:

    widgets_class = []
    widgets_instance = []

    @classmethod
    def set_request(cls, user):
        if len(cls.widgets_class) == 0:
            cls.widgets_class = [clss for clss in BaseWidget.__subclasses__()]

        cls.widgets_instance = []
        for wid in cls.widgets_class:
            cls.widgets_instance.append(wid.instantiate(user))
