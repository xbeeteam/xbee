from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View

from xbee_company.models import Company


@method_decorator(login_required, name='dispatch')
class BaseClassView(View):
    def get_companies_by_user(self, django_request):
        the_user = django_request.user
        sub_user_type = the_user.cast()  # can be an instance of Reseller or Customer
        if the_user.has_perm('xbee_account.xbee_perm_reseller_admin'):
            if sub_user_type.selected_company is None:
                comps = Company.objects.all()
            else:
                comps = Company.objects.filter(pk=sub_user_type.selected_company.pk)
        elif the_user.has_perm('xbee_account.xbee_perm_reseller'):
            if sub_user_type.selected_company is None:
                comps = sub_user_type.get_companies()
            else:
                comps = Company.objects.filter(pk=sub_user_type.selected_company.pk)
        elif the_user.has_perm('xbee_account.xbee_perm_client'):
            comps = Company.objects.filter(pk=sub_user_type.company.pk)

        return comps