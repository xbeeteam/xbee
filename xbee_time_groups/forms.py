from django.forms import ModelForm, formset_factory

from xbee_time_groups.models import TimeGroup, Time


class TimeGroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps

    class Meta:
        model = TimeGroup
        fields = '__all__'


class TimeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['weekday_start'].new_group = 'true'
        self.fields['month_start'].new_group = 'true'
        self.fields['year_start'].new_group = 'true'
        self.fields['time_zone'].required = False

    class Meta:
        model = Time
        fields = '__all__'
        exclude = ('time_group', )


TimeFormset = formset_factory(TimeForm)
