from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("Time Groups"), reverse('xbee_time_groups:tg_list'), weight=700,
                               icon="iconsminds-clock-forward"))
