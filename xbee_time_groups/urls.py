from django.urls import path

from xbee_time_groups import views

app_name = 'xbee_time_groups'

urlpatterns = [
    path('', views.TimeGroupsView.as_view(), name='tg_list'),
    path('detail/<int:id_tg>', views.TimeGroupDetailView.as_view(), name='tg_detail'),
    path('delete/<int:id_tg>', views.DeleteTimeGroupView.as_view(), name='delete_tg'),
    path('time/delete/<int:id_t>', views.DeleteTimeView.as_view(), name='delete_time'),

]
