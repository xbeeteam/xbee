import pytz
from django.db import models
from django.urls import reverse
from django.utils.dates import MONTHS, WEEKDAYS


class TimeGroup(models.Model):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_timegroups", db_column='tg_comp_id')
    description = models.TextField(db_column='tg_description')

    def __str__(self):
        return f"{self.description} [{self.company.name}]"

    def get_absolute_url(self):
        return reverse("xbee_time_groups:tg_detail", kwargs={"id_tg": self.pk})

    class Meta:
        verbose_name = 'TimeGroup'
        verbose_name_plural = 'TimeGroups'
        db_table = 'xbee_timegroup'


class Time(models.Model):
    time_group = models.ForeignKey(TimeGroup, null=False, on_delete=models.CASCADE, related_name="times",
                                   db_column='tm_tg_id')
    time_start = models.TimeField(db_column='tm_time_start')
    time_finish = models.TimeField(db_column='tm_time_finish')
    weekday_start = models.IntegerField(choices=list(WEEKDAYS.items()), db_column='tm_weekday_start')
    weekday_finish = models.IntegerField(choices=list(WEEKDAYS.items()), db_column='tm_weekday_finish')
    month_start = models.IntegerField(choices=list(MONTHS.items()), db_column='tm_month_start')
    month_finish = models.IntegerField(choices=list(MONTHS.items()), db_column='tm_month_finish')

    TIMEZONES = ((None, '---------'), ) + tuple(zip(pytz.all_timezones, pytz.all_timezones))
    time_zone = models.CharField(max_length=60, choices=TIMEZONES, null=True, default=None, db_column='tm_time_zone')
    year_start = models.IntegerField(db_column='tm_year_start')
    year_finish = models.IntegerField(db_column='tm_year_end')

    def __str__(self):
        return f"{self.weekday_start}:{self.weekday_finish} | {self.month_start}:{self.month_finish}" \
               f" [{self.time_group.description}]"

    class Meta:
        verbose_name = 'Time'
        verbose_name_plural = 'Times'
        db_table = 'xbee_time'
