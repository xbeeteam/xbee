import sys

from django.apps import AppConfig


class XbeeTimeGroupsConfig(AppConfig):
    name = 'xbee_time_groups'
    urls = 'time-groups'
    context = 'timegroups'
    extension = 's'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Time Groups')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_time_groups.models import TimeGroup

                Module.objects.create(
                    name='Time Groups',
                    context=self.context,
                    extension=self.extension,
                    model_object=ContentType.objects.get_for_model(TimeGroup)
                ).save()
