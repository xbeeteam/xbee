from django.contrib import admin

from xbee_time_groups.models import Time, TimeGroup

admin.site.register(TimeGroup)
admin.site.register(Time)
