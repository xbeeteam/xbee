from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_company.models import Company
from xbee_time_groups.forms import TimeGroupForm, TimeFormset
from xbee_time_groups.models import TimeGroup, Time


class TimeGroupsView(LoginRequiredMixin, BaseClassView):
    """
        List all time groups and allows to create a new one
    """

    template_name = 'xbee_time_groups/tg_list.html'
    redirect_url = 'xbee_time_groups:tg_list'

    def post(self, request):
        form = TimeGroupForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _("Time group created!"))
            return redirect(form.instance)
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        form_tg = TimeGroupForm(initial={'companies': comps})
        tgs = TimeGroup.objects.filter(company__in=comps)
        return render(request, self.template_name, {'tgs': tgs, 'form_tg': form_tg})


class TimeGroupDetailView(LoginRequiredMixin, BaseClassView):
    """
        Detail of a time group, this view allows to edit the details of the time group and create it's time
    """

    template_name = 'xbee_time_groups/tg_detail.html'

    def post(self, request, id_tg):
        tg = get_object_or_404(TimeGroup, pk=id_tg)
        if 'tg_detail_submitted' in request.POST:
            form = TimeGroupForm(request.POST, instance=tg)
            if form.is_valid():
                form.save()
                messages.success(request, _("Time group updated!"))
                return redirect(form.instance)
            else:
                messages.error(request, _("Somethings goes wrong!"))

        elif 'fs_t_submitted' in request.POST:
            formset = TimeFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    if form.is_valid():
                        t = form.save(commit=False)
                        t.time_group = tg
                        t.save()
                        messages.success(
                            request,
                            _(f"Time created: "
                              f"{t.get_month_start_display()} - {t.get_month_finish_display()} | "
                              f"{t.get_weekday_start_display()} - {t.get_weekday_finish_display()} "
                              f"[{t.time_start} / {t.time_finish}]")
                        )

        return redirect(tg)

    def get(self, request, id_tg):
        tg = get_object_or_404(TimeGroup, pk=id_tg)
        comps = Company.objects.filter(pk=tg.company.pk)
        form_tg_detail = TimeGroupForm(instance=tg, initial={'companies': comps})
        t_formset = TimeFormset()

        return render(request, self.template_name, {
            'tg': tg,
            'form_tg_detail': form_tg_detail,
            't_formset': t_formset
        })


class DeleteTimeGroupView(LoginRequiredMixin, BaseClassView):
    """
        delete time group by id
    """

    redirect_url = 'xbee_time_groups:tg_list'

    def get(self, request, id_tg):
        TimeGroup.objects.get(pk=id_tg).delete()
        messages.success(request, _("Deleted!"))

        return redirect(self.redirect_url)


class DeleteTimeView(LoginRequiredMixin, BaseClassView):
    """
        delete time by id
    """

    def get(self, request, id_t):
        t = Time.objects.get(pk=id_t)
        tg = t.time_group
        t.delete()
        messages.success(request, _("Deleted!"))

        return redirect(tg)
