from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_company.models import Company
from xbee_queues.forms import QueueForm
from xbee_queues.models import Queue


class QueuesView(LoginRequiredMixin, BaseClassView):
    """
        List all queue and allows to create a new one
    """

    template_name = 'xbee_queues/queues_list.html'
    redirect_url = 'xbee_queues:queues_list'

    def get(self, request):
        comps = self.get_companies_by_user(request)
        queues = Queue.objects.filter(company__in=comps)
        form_queue = QueueForm(initial={'companies': comps})

        return render(request, self.template_name, {'form_queue': form_queue, 'queues': queues})

    def post(self, request):
        form = QueueForm(request.POST)
        if form.is_valid():
            form.save()

            messages.success(request, _("Queue created!"))
            return redirect(form.instance)
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)


class QueueDetailView(LoginRequiredMixin, BaseClassView):
    """
        Detail of a queue, this view allows to edit the details of the queue
    """

    template_name = 'xbee_queues/queue_detail.html'

    def post(self, request, id_queue):
        queue = get_object_or_404(Queue, pk=id_queue)
        if 'queue_submitted' in request.POST:
            form = QueueForm(request.POST, instance=queue)
            if form.is_valid():
                form.save()
                messages.success(request, _("Queue updated!"))
                return redirect(form.instance)
            else:
                messages.error(request, _("Somethings goes wrong!"))

        return redirect(queue)

    def get(self, request, id_queue):
        queue = get_object_or_404(Queue, pk=id_queue)
        comps = Company.objects.filter(pk=queue.company.pk)
        form_queue = QueueForm(instance=queue, initial={'companies': comps})

        return render(request, self.template_name, {'queue': queue, 'form_queue': form_queue})


class DeleteQueueView(LoginRequiredMixin, BaseClassView):
    """
        delete queue by id
    """

    redirect_url = 'xbee_queues:queues_list'

    def get(self, request, id_queue):
        Queue.objects.get(pk=id_queue).delete()
        messages.success(request, _("Deleted!"))

        return redirect(self.redirect_url)
