from django.db import models
from django.urls import reverse

from django.utils.translation import ugettext_lazy as _

from xbee_modules.models import PluggableModuleModel


class Queue(PluggableModuleModel):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_queues", db_column='que_comp_id')
    number = models.IntegerField(default=0, db_column='que_number')
    name = models.CharField(max_length=64, db_column='que_name')
    max_wait_time = models.PositiveIntegerField(default=0, db_column='que_max_wait_time')
    agent_timeout = models.PositiveIntegerField(default=0, db_column='que_agent_timeout')
    retry = models.PositiveIntegerField(db_column='que_retry')
    music_on_hold = models.ForeignKey('xbee_recordings.MusicOnHoldingCategory', null=False, on_delete=models.PROTECT,
                                      db_column='que_music_on_hold')

    QUEUE_CALL_REC_CHOICES = (
        ('Y', _('Yes')),
        ('N', _('No')),
    )
    call_recording = models.CharField(default='N', choices=QUEUE_CALL_REC_CHOICES, max_length=2,
                                      db_column='que_call_recording')
    max_callers = models.PositiveIntegerField(default=0, db_column='que_max_callers')

    MARK_ANSWERED_CHOICES = (
        ('Y', _('Yes')),
        ('N', _('No')),
    )
    mark_calls_answered_elsewhere = models.CharField(default='N', choices=MARK_ANSWERED_CHOICES, max_length=2,
                                                     db_column='que_mark_calls_answered_elsewhere')
    retry_message = models.PositiveIntegerField(default=0, db_column='que_retry_message')
    static_agents = models.TextField(db_column='que_static_agents')
    prepend_cid = models.CharField(default=None, null=True, blank=True, max_length=50, db_column='que_prepend_cid')

    def get_absolute_url(self):
        return reverse("xbee_queues:queue_detail", kwargs={"id_queue": self.pk})

    def __str__(self):
        return f"{self.name} [{self.company.name}]"

    class Meta:
        verbose_name = 'Queue'
        verbose_name_plural = 'Queues'
        db_table = 'xbee_queue'
