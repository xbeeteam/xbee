import sys

from django.apps import AppConfig


class XbeeQueuesConfig(AppConfig):
    name = 'xbee_queues'
    urls = 'queues'
    context = 'queues'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Queues')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_queues.models import Queue

                Module.objects.create(
                    name='Queues',
                    context=self.context,
                    model_object=ContentType.objects.get_for_model(Queue)
                ).save()
