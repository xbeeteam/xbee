from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("Queues"), reverse('xbee_queues:queues_list'), weight=600,
                               icon="iconsminds-arrow-shuffle"))
