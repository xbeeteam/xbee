from xbee_modules.forms import PluggableModuleModelForm
from xbee_queues.models import Queue
from xbee_recordings.models import MusicOnHoldingCategory


class QueueForm(PluggableModuleModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['music_on_hold'].queryset = MusicOnHoldingCategory.objects.filter(company__in=comps)

        self.fields['static_agents'].new_group = 'true'

    class Meta:
        model = Queue
        fields = '__all__'
        exclude = PluggableModuleModelForm.Meta.exclude
