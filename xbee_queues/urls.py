from django.urls import path

from xbee_queues import views

app_name = 'xbee_queues'

urlpatterns = [
    path('', views.QueuesView.as_view(), name='queues_list'),
    path('detail/<int:id_queue>', views.QueueDetailView.as_view(), name='queue_detail'),
    path('delete/<int:id_queue>', views.DeleteQueueView.as_view(), name='delete_queue'),

]
