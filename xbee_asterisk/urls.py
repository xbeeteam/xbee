from django.urls import path

from xbee_asterisk import views

app_name = 'xbee_asterisk'

urlpatterns = [
    path('', views.AsterisksView.as_view(), name='asterisk_list'),
    path('delete/<int:ast_id>', views.DeleteAsteriskView.as_view(), name='delete_asterisk'),
    path('apply-config/', views.ApplyConfigView.as_view(), name='apply_config'),

]
