from django.apps import AppConfig


class XbeeAsteriskConfig(AppConfig):
    name = 'xbee_asterisk'
    urls = 'asterisk'
