from django.forms import ModelForm

from xbee_asterisk.models import Asterisk


class AsteriskForm(ModelForm):

    class Meta:
        model = Asterisk
        fields = '__all__'
        exclude = ('reseller', )
