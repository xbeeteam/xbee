from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db.models import ProtectedError
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.views import View

from utils.base_class_view import BaseClassView
from xbee_asterisk.forms import AsteriskForm
from xbee_asterisk.models import Asterisk
from xbee_extensions.models import Extension, SipFriends


class AsterisksView(LoginRequiredMixin, BaseClassView):
    """
        This view show to a reseller all asterisk and allow to create a new one
    """

    template_name = 'xbee_asterisk/asterisk_list.html'
    redirect_url = 'xbee_asterisk:asterisk_list'

    def dispatch(self, request, *args, **kwargs):
        if not (request.user.has_perm('xbee_account.xbee_perm_reseller_admin') or
                request.user.has_perm('xbee_account.xbee_perm_reseller')):
            raise PermissionDenied
        return super(AsterisksView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        reseller = request.user.cast()
        if request.user.has_perm('xbee_account.xbee_perm_reseller_admin'):
            asterisks = Asterisk.objects.all()
        elif request.user.has_perm('xbee_account.xbee_perm_reseller'):
            asterisks = reseller.get_asterisks()

        form_asterisk = AsteriskForm()
        return render(request, self.template_name,
                      {'form_asterisk': form_asterisk, 'asterisks': asterisks})

    def post(self, request):
        reseller = request.user.cast()
        if 'asterisk_submitted' in request.POST:
            form = AsteriskForm(request.POST)
            if form.is_valid():
                new_asterisk = form.save(commit=False)
                new_asterisk.reseller = reseller.seller if reseller.seller else reseller
                new_asterisk.save()
                messages.success(request, _("Asterisk created!"))
            else:
                messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)


class DeleteAsteriskView(LoginRequiredMixin, BaseClassView):
    """
        View for delete asterisk by id
    """

    redirect_url = 'xbee_asterisk:asterisk_list'

    def get(self, request, ast_id):
        try:
            Asterisk.objects.get(pk=ast_id).delete()
            messages.success(request, _("Deleted!"))
        except ProtectedError as e:
            context = {'queryset': e.protected_objects}
            html_content = render_to_string('exception-message-template.html', context)
            messages.warning(
                request,
                _("This Asterisk is associated with a Company, please remove the association and try again.")
                + html_content
            )

        return redirect(self.redirect_url)


class ApplyConfigView(LoginRequiredMixin, View):
    def get(self, request):
        # sip friends
        exts = Extension.objects.all()
        SipFriends.objects.all().delete()
        for ext in exts:
            if ext.technology == 'sip':
                obj = ext.parameters
                obj['secret'] = ext.secret
                obj['context'] = ext.context
                obj['name'] = ext.name
                sf = SipFriends(**obj)
                sf.save()

        # todo avvia lo script in background
        messages.success(request, _("Configuration applied"))
        return redirect('xbee_account:dashboard')
