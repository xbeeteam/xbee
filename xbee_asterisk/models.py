from django.db import models


class Asterisk(models.Model):
    ip = models.CharField(default=None, blank=True, null=True, max_length=15, db_column='ast_ip')
    admin_login_user = models.CharField(default=None, blank=True, null=True, max_length=120,
                                        db_column='ast_admin_login_user')
    admin_login_password = models.CharField(default=None, blank=True, null=True, max_length=120,
                                            db_column='ast_admin_login_password')
    reseller = models.ForeignKey('xbee_account.Reseller', on_delete=models.CASCADE, related_name='reseller_asterisks',
                                 db_column='ast_reseller')

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = 'Asterisk'
        verbose_name_plural = 'Asterisks'
        db_table = 'xbee_asterisk'
