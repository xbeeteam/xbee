from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


Menu.add_item(
    "main",
    MenuItem(
        _("Asterisk"),
        reverse('xbee_asterisk:asterisk_list'),
        weight=20,
        icon="iconsminds-snow",
        check=lambda request: request.user.has_perm('xbee_account.xbee_perm_reseller_admin') or
                              request.user.has_perm('xbee_account.xbee_perm_reseller'))
)

# dropdown smart actions
Menu.add_item(
    "dropdown_smart_actions",
    MenuItem(
        _("Apply Config"),
        reverse('xbee_asterisk:apply_config'),
        icon="simple-icon-check",
        weight=10
    )
)
