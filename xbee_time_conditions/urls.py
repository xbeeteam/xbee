from django.urls import path

from xbee_time_conditions import views

app_name = 'xbee_time_conditions'

urlpatterns = [
    path('', views.TimeConditionsView.as_view(), name='tc_list'),
    path('delete/<int:id_tc>', views.DeleteTimeConditionView.as_view(), name='delete_tc'),

]
