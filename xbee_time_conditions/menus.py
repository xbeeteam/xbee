from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("Time Conditions"), reverse('xbee_time_conditions:tc_list'), weight=800,
                               icon="iconsminds-arrow-merge"))
