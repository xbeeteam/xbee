from django import forms
from django.forms import ModelForm
from django.urls import reverse_lazy

from xbee_modules.models import Module
from xbee_time_conditions.models import TimeCondition


class TimeConditionForm(ModelForm):

    module_matches = forms.ModelChoiceField(queryset=Module.objects.all(), required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
    }))
    sub_module_matches = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    module_non_matches = forms.ModelChoiceField(queryset=Module.objects.all(), required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
    }))
    sub_module_non_matches = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps
                mdls = Module.objects.filter(
                    company_modules__in=[compp.pk for compp in comps]
                ).distinct()
                self.fields['module_matches'].queryset = mdls
                self.fields['module_non_matches'].queryset = mdls

    def save(self, commit=True):
        tc_obj = super().save(commit=False)
        if self.cleaned_data['module_matches'] and self.cleaned_data['sub_module_matches']:
            model_class_module = self.cleaned_data['module_matches'].model_object.model_class()
            tc_obj.destination_matches = model_class_module.objects.get(pk=self.cleaned_data['sub_module_matches'])

        if self.cleaned_data['module_non_matches'] and self.cleaned_data['sub_module_non_matches']:
            model_class_module = self.cleaned_data['module_non_matches'].model_object.model_class()
            tc_obj.destination_non_matches = model_class_module.objects.get(
                pk=self.cleaned_data['sub_module_non_matches']
            )

        if commit:
            tc_obj.save()

        return tc_obj

    class Meta:
        model = TimeCondition
        fields = '__all__'
        exclude = ('destination_matches_content_type', 'destination_matches_object_id',
                   'destination_non_matches_content_type', 'destination_non_matches_object_id', )
