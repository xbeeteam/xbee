from django.contrib import admin

from xbee_time_conditions.models import TimeCondition

admin.site.register(TimeCondition)
