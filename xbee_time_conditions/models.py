from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class TimeCondition(models.Model):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_timeconditions", db_column='tc_comp_id')

    name = models.CharField(max_length=64, null=False, db_column='tc_name')
    time_group = models.ForeignKey('xbee_time_groups.TimeGroup', null=False, on_delete=models.PROTECT,
                                   related_name='+', db_column='tc_tg_id')

    destination_matches_content_type = models.ForeignKey(ContentType, default=None, null=True,
                                                         related_name='+', on_delete=models.CASCADE)
    destination_matches_object_id = models.PositiveIntegerField(default=0)
    destination_matches = GenericForeignKey('destination_matches_content_type', 'destination_matches_object_id')

    destination_non_matches_content_type = models.ForeignKey(ContentType, default=None, null=True,
                                                             related_name='+', on_delete=models.CASCADE)
    destination_non_matches_object_id = models.PositiveIntegerField(default=0)
    destination_non_matches = GenericForeignKey('destination_non_matches_content_type',
                                                'destination_non_matches_object_id')

    def __str__(self):
        return f"{self.name} [{self.company.name}]"

    class Meta:
        verbose_name = 'TimeCondition'
        verbose_name_plural = 'TimeConditions'
        db_table = 'xbee_timecondition'
