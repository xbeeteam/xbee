import sys

from django.apps import AppConfig


class XbeeTimeConditionsConfig(AppConfig):
    name = 'xbee_time_conditions'
    urls = 'time-conditions'
    context = 'tc'
    extension = 's'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Time Conditions')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_time_conditions.models import TimeCondition

                Module.objects.create(
                    name='Time Conditions',
                    context=self.context,
                    extension=self.extension,
                    model_object=ContentType.objects.get_for_model(TimeCondition)
                ).save()
