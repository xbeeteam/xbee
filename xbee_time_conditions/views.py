from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect

from utils.base_class_view import BaseClassView
from xbee_time_conditions.forms import TimeConditionForm
from xbee_time_conditions.models import TimeCondition

from django.utils.translation import ugettext_lazy as _


class TimeConditionsView(LoginRequiredMixin, BaseClassView):
    """
        List all time conditions and allows to create a new one
    """

    template_name = 'xbee_time_conditions/tc_list.html'
    redirect_url = 'xbee_time_conditions:tc_list'

    def post(self, request):
        form = TimeConditionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _("Time group created!"))
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        form_tc = TimeConditionForm(initial={'companies': comps})
        tcs = TimeCondition.objects.filter(company__in=comps)

        return render(request, self.template_name, {'form_tc': form_tc, 'tcs': tcs})


class DeleteTimeConditionView(LoginRequiredMixin, BaseClassView):
    """
        delete time condition by id
    """

    redirect_url = 'xbee_time_conditions:tc_list'

    def get(self, request, id_tc):
        TimeCondition.objects.get(pk=id_tc).delete()
        messages.success(request, _("Deleted!"))

        return redirect(self.redirect_url)
