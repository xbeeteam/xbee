import os
from configparser import ConfigParser

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base


if os.environ['XBEE_CONFIG_DEBUG'] == 'True':
    configpath = '/home/antonio/PycharmProjects/xbee/config_script/'
else:
    configpath = '/opt/xbee/config_script/'

Model = declarative_base(name='Model')

config = ConfigParser()
config.read(f'{configpath}config.ini')

connection_str = f'postgresql://{config["DATABASE"]["user"]}:{config["DATABASE"]["password"]}@' \
                 f'{config["DATABASE"]["host"]}:{config["DATABASE"]["port"]}/{config["DATABASE"]["name"]}'
engine = create_engine(connection_str)
