ASTERISK_WEEKDAYS = {0: 'mon', 1: 'tue', 2: 'wed', 3: 'thu', 4: 'fri', 5: 'sat', 6: 'sun'}

ASTERISK_MONTHS = {1: 'jan', 2: 'feb', 3: 'mar', 4: 'apr', 5: 'may', 6: 'jun', 7: 'jul', 8: 'aug', 9: 'sep',
                   10: 'oct', 11: 'nov', 12: 'dec'}

PATH_ASTERISK_SOUNDS = '/var/lib/asterisk/sounds/'
PATH_ASTERISK_MOH = '/var/lib/asterisk/moh/'  # {comp_id}-{category_name}
