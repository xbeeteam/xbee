import os

from sqlalchemy import text
from sqlalchemy.orm import sessionmaker

import models_definition as md


def generate_asterisk_configuration():
    Session = sessionmaker(bind=md.conn.engine)
    asession = Session()

    local_ip = md.conn.config["LOCAL"]["ip"]
    comps = asession.query(md.Company)\
        .join(md.Company.asterisk)\
        .filter(
            text(f"{md.Company.__table__.name}.comp_enabled=true"),
            text(f"{md.Asterisk.__table__.name}.ast_ip='{local_ip}'")
        )

    if os.environ['XBEE_CONFIG_DEBUG']:
        base_path = './'
    else:
        base_path = '/etc/asterisk/'
    include_ext = ''

    # TODO cancella i file di conf generati precedentemente
    md.get_generic_object(asession, 33, 9)  # todo cancella, è un test

    for comp in comps:
        comp_id = str(comp.comp_id).zfill(11)  # TODO fai il controllo per vision

        ext_company = ''
        include_ext += f'#include extensions_company_{comp_id}.conf ; {comp.comp_name}\n'

        print(f'\n\nCOMPANY: {comp.comp_name} - {comp_id}')
        print(f'Reseller:  {comp.comp_name}: {comp.reseller.base_user.username}')

        print(f'Customer [{comp.comp_name}]:')
        for cust in comp.customers:
            print(f'- {cust.base_user.username}')

        print(f'\nModules [{comp.comp_name}]:')
        for modl in comp.modules:
            if modl.module.mod_name == 'Inbound routes' and len(comp.inbounds) > 0:
                print(f'\nInbounds ({len(comp.inbounds)}) [{comp.comp_name}]:')
                inb_conf = md.Inbound.head_extension(comp_id)
                for inb in comp.inbounds:
                    inb_conf += inb.extension_body(comp_id)
                inb_conf += md.Inbound.footer_extension()
                # print(inb_conf)
                ext_company += inb_conf + '\n'

            if modl.module.mod_name == 'Outbound routes' and len(comp.outbounds) > 0:
                print(f'\nOutbound ({len(comp.outbounds)}) [{comp.comp_name}]:')
                out_conf = md.Outbound.head_extension(comp_id, comp.outbounds)
                for inb in comp.outbounds:
                    out_conf += inb.extension_body(comp_id)
                # print(out_conf)
                ext_company += out_conf + '\n'

            elif modl.module.mod_name == 'MusicOnHolding' and len(comp.music_on_holding_categories) > 0:
                print(f'\nMusicOnHoldCategory ({len(comp.music_on_holding_categories)}) [{comp.comp_name}]:')
                for mohc in comp.music_on_holding_categories:
                    print(f'- {mohc.mohc_name}')

            elif modl.module.mod_name == 'Announcements' and len(comp.announcements) > 0:
                print(f'\nAnnouncements ({len(comp.announcements)}) [{comp.comp_name}]:')
                ann_conf = ''
                for ann in comp.announcements:
                    ann_conf += ann.extension_body(comp_id)
                # print(ann_conf)
                ext_company += ann_conf + '\n'

            elif modl.module.mod_name == 'Time Conditions' and len(comp.timeconditions) > 0:
                print(f'\nTime Conditions ({len(comp.timeconditions)}) [{comp.comp_name}]:')
                tc_conf = ''
                for tcs in comp.timeconditions:
                    tc_conf += tcs.extension_body(comp_id)
                # print(tc_conf)
                ext_company += tc_conf + '\n'

            elif modl.module.mod_name == 'Queues' and len(comp.queues) > 0:
                print(f'\nQueues ({len(comp.queues)}) [{comp.comp_name}]:')
                que_conf = ''
                for que in comp.queues:
                    que_conf += que.extension_body(comp_id)
                # print(que_conf)
                ext_company += que_conf + '\n'

            elif modl.module.mod_name == 'Call Switch Conditions' and len(comp.callswitchconditions) > 0:
                print(f'\nCall Switch Conditions ({len(comp.callswitchconditions)}) [{comp.comp_name}]:')
                csc_conf = ''
                for csc in comp.callswitchconditions:
                    csc_conf += csc.extension_body(comp_id)
                # print(csc_conf)
                ext_company += csc_conf + '\n'

        # generate config foreach company
        path = f'{base_path}extensions_company_{comp_id}.conf'
        with open(path, "w") as f:
            f.write(ext_company)

    # generate include file
    path = f'{base_path}extensions_companies.conf'
    with open(path, "w") as f:
        f.write(include_ext)

    # close the db session
    asession.close()


if __name__ == '__main__':
    generate_asterisk_configuration()


'''
--> context degli users

[00000000001-users]
exten => 1001,1,Macro(dial-user,1001,00000000001)
exten => 1001,n,Hangup
exten => 1002,1,Macro(dial-user,1002,00000000001)
exten => 1002,n,Hangup
exten => 251,1,Macro(dial-user,251,00000000001)
exten => 251,n,Hangup

exten => h,1,Hangup

include => 00000000001-member-hint

[00000000001-users-dial-string]
exten => 1001,1,Set(XBEE-USER-DIAL-STRING=SIP/00000000001-1001)
exten => 1001,n,Return()
exten => 1002,1,Set(XBEE-USER-DIAL-STRING=SIP/00000000001-1002)
exten => 1002,n,Return()
exten => 251,1,Set(XBEE-USER-DIAL-STRING=SIP/00000000001-251)
exten => 251,n,Return()
'''