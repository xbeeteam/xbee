import sys

from sqlalchemy import Table, MetaData, text
from sqlalchemy.orm import relationship

sys.path.append('/opt/xbee/config_script/')

import const as c
import db_connection as conn


class Company(conn.Model):
    __table__ = Table(
        'xbee_company',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    reseller = relationship(
        "Reseller",
        backref='reseller_company',
        primaryjoin="Company.comp_reseller == Reseller.xbeeuser_ptr_id",
        foreign_keys="Company.comp_reseller"
    )
    asterisk = relationship(
        "Asterisk",
        backref='company_asterisk',
        primaryjoin="Company.comp_asterisk_id == Asterisk.id",
        foreign_keys="Company.comp_asterisk_id"
    )


class Asterisk(conn.Model):
    __table__ = Table(
        'xbee_asterisk',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


class Reseller(conn.Model):
    __table__ = Table(
        'xbee_account_reseller',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    base_user = relationship(
        "XbeeUser",
        backref='reseller_user',
        primaryjoin="Reseller.xbeeuser_ptr_id == XbeeUser.id",
        foreign_keys="Reseller.xbeeuser_ptr_id"
    )


class Customer(conn.Model):
    __table__ = Table(
        'xbee_account_customer',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    base_user = relationship(
        "XbeeUser",
        backref='customer_user',
        primaryjoin="Customer.xbeeuser_ptr_id == XbeeUser.id",
        foreign_keys="Customer.xbeeuser_ptr_id"
    )
    company = relationship(
        "Company",
        backref='customers',
        primaryjoin="Customer.usr_comp_id == Company.comp_id",
        foreign_keys="Customer.usr_comp_id"
    )


class XbeeUser(conn.Model):
    __table__ = Table(
        'xbee_account_xbeeuser',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


class MusicOnHoldCategory(conn.Model):
    __table__ = Table(
        'xbee_musiconholdingcategory',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='music_on_holding_categories',
        primaryjoin="MusicOnHoldCategory.mohc_comp_id == Company.comp_id",
        foreign_keys="MusicOnHoldCategory.mohc_comp_id"
    )


class Announcements(conn.Model):
    __table__ = Table(
        'xbee_announcement',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    recording = relationship(
        "SystemRecording",
        backref='announcement',
        primaryjoin="Announcements.ann_recording_id == SystemRecording.recording_ptr_id",
        foreign_keys="Announcements.ann_recording_id"
    )
    company = relationship(
        "Company",
        backref='announcements',
        primaryjoin="Announcements.ann_comp_id == Company.comp_id",
        foreign_keys="Announcements.ann_comp_id"
    )

    def extension_body(self, comp_id):
        ext = f'[{comp_id}-announcement-{self.id}] ; ANNOUNCEMENT: {self.ann_description}\n'
        ext += f'exten => s,1,NoOp(ANNOUNCEMENT: {self.ann_description})\n'
        if self.ann_answer_channel == 'Y':
            ext += 'exten => s,n,Answer\n'

        # write the recoding file to the filesystem
        rel_path = f'xbee/{comp_id}/announcement-{self.id}.wav'
        abs_file_path = f"{c.PATH_ASTERISK_SOUNDS}{rel_path}"
        try:
            with open(abs_file_path, "wb") as f:
                f.write(self.recording.get_file())
        except:
            print('--> Writing file error!')

        ext += f'exten => s,n,Playback({rel_path})\n'
        # TODO sistema la destinazione!!
        ext += 'exten => s,n,Goto(00000000001-queues,12000,1)\n\n'
        ext += 'exten => h,1,Hangup\n\n'

        return ext


class Inbound(conn.Model):
    __table__ = Table(
        'xbee_inbound',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='inbounds',
        primaryjoin="Inbound.inb_comp_id == Company.comp_id",
        foreign_keys="Inbound.inb_comp_id"
    )

    @staticmethod
    def head_extension(comp_id):
        return f"[{comp_id}-inbound]\n"

    def extension_body(self, comp_id):
        did = self.inb_did if self.inb_did.isdigit() else f"_{self.inb_did}"
        ext = f'exten => {did},1,Set(CHANNEL(hangup_handler_push)={comp_id}-hangup,s,1)\n'
        ext += f'exten => {did},n,Set(XBEE-CALLTYPE=1)\n'
        ext += f'exten => {did},n,Set(XBEE-FROM-DID=${{EXTEN}})\n'
        ext += f'exten => {did},n,Macro(prepend-cid,{self.inb_prepend_cid}-)\n'
        # TODO se la destination è diversa da hangup ci vuole il gotoif
        # TODO fix del context a seconda della destination
        ext += f'exten => {did},n,'
        ext += f'GotoIf($[${{DIALPLAN_EXISTS({comp_id}-queues,12000,1)}}]?{comp_id}-queues,12000,1)\n'
        ext += f'exten => {did},n(Hangup),Hangup\n\n'

        return ext

    @staticmethod
    def footer_extension():
        return 'exten => h,1,Hangup'


class Module(conn.Model):
    __table__ = Table(
        'xbee_module',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


class CompanyModules(conn.Model):
    __table__ = Table(
        'xbee_company_modules',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='modules',
        primaryjoin="CompanyModules.company_id == Company.comp_id",
        foreign_keys="CompanyModules.company_id"
    )
    module = relationship(
        "Module",
        backref='attached_companies',
        primaryjoin="CompanyModules.module_id == Module.mod_id",
        foreign_keys="CompanyModules.module_id"
    )


class TimeCondition(conn.Model):
    __table__ = Table(
        'xbee_timecondition',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    time_group = relationship(
        "TimeGroup",
        backref='time_condition',
        primaryjoin="TimeCondition.tc_tg_id == TimeGroup.id",
        foreign_keys="TimeCondition.tc_tg_id"
    )
    company = relationship(
        "Company",
        backref='timeconditions',
        primaryjoin="TimeCondition.tc_comp_id == Company.comp_id",
        foreign_keys="TimeCondition.tc_comp_id"
    )

    def extension_body(self, comp_id):
        ext = f'[{comp_id}-tc-{self.id}] ; TC: {self.tc_name}\n'
        ext += f'exten => s,1,NoOp(TC: {self.tc_name})\n'
        # loop on timegroup times
        for tm in self.time_group.times:
            days = f'{c.ASTERISK_WEEKDAYS[tm.tm_weekday_start]}-{c.ASTERISK_WEEKDAYS[tm.tm_weekday_finish]}' \
                if tm.tm_weekday_start is not None else '*'
            months = f'{tm.tm_month_start}-{tm.tm_month_finish}' if tm.tm_month_start else '*'
            tz = f',{tm.tm_time_zone}?' if tm.tm_time_zone else '?'

            ext += 'exten => s,n,' \
                   f'GotoIfTime({tm.tm_time_start.hour}:{tm.tm_time_start.minute}-' \
                   f'{tm.tm_time_finish.hour}:{tm.tm_time_finish.minute},' \
                   f'{days},{months}' \
                   f'{tz}announcement-1,s,1) ; {self.tc_name}\n'  # TODO announcmeent deve essere la destinazione!!
        # TODO la destination not match
        # exten => s,n,Goto(announcement-2,s,1)
        return ext


class TimeGroup(conn.Model):
    __table__ = Table(
        'xbee_timegroup',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


class Time(conn.Model):
    __table__ = Table(
        'xbee_time',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    time_group = relationship(
        "TimeGroup",
        backref='times',
        primaryjoin="Time.tm_tg_id == TimeGroup.id",
        foreign_keys="Time.tm_tg_id"
    )


class SystemRecording(conn.Model):
    __table__ = Table(
        'xbee_systemrecording',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    recording = relationship(
        "Recording",
        backref='sys_recording',
        primaryjoin="SystemRecording.recording_ptr_id == Recording.id",
        foreign_keys="SystemRecording.recording_ptr_id"
    )

    def get_file(self):
        return self.recording.file.recfl_converted_file


class Recording(conn.Model):
    __table__ = Table(
        'xbee_recording',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    file = relationship(
        "RecordingFile",
        backref='recording',
        primaryjoin="Recording.id == RecordingFile.recfl_rec_id",
        foreign_keys="Recording.id"
    )


class RecordingFile(conn.Model):
    __table__ = Table(
        'xbee_recordingfile',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


class Queue(conn.Model):
    __table__ = Table(
        'xbee_queue',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='queues',
        primaryjoin="Queue.que_comp_id == Company.comp_id",
        foreign_keys="Queue.que_comp_id"
    )

    def extension_body(self, comp_id):
        ext = f'[{comp_id}-queues]\n'
        ext += f'; queue: {self.que_name}\n'
        ext += f'exten => {self.que_number},1,Answer\n'
        ext += f'exten => {self.que_number},n,Macro(prepend-cid,{self.que_prepend_cid})\n'
        ext += f'exten => {self.que_number},n,QueueLog({comp_id}-{self.que_number},' \
               '${{UNIQUEID}},NONE,DID,${{FROM_DID}})\n'

        mark_answered = ''
        if self.que_mark_calls_answered_elsewhere == 'Y':
            mark_answered = 'C'
        ext += f'exten => {self.que_number},n,Queue({comp_id}-{self.que_number},{mark_answered}' \
               f',,custom/sticazzi.wav,{self.que_max_wait_time},,,,,)\n'  # TODO ottieni il wav della moh
        ext += f'exten => {self.que_number},n,GotoIf($[${{DIALPLAN_EXISTS(sticazzix2,s,1)}}]?sticazzix2,s,1)\n'  # TODO destinazione
        ext += f'exten => {self.que_number},n,Hangup\n\n'
        ext += 'exten => h,1,Hangup\n\n'
        ext += 'include => 00000000001-member-hint\n\n'

        ext += f'[{comp_id}-queues-dial-{self.que_number}]\n'
        for stat_ag in self.que_static_agents.splitlines():
            ag = stat_ag.split(',')
            ext += f'exten => {comp_id}-{ag[0]},1,Dial(SIP/{comp_id}-{ag[0]})\n'
        ext += '\nexten => h,1,Hangup\n\n'
        ext += 'include => 00000000001-member-hint\n\n'

        return ext


class Outbound(conn.Model):
    __table__ = Table(
        'xbee_outbound',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='outbounds',
        primaryjoin="Outbound.outb_comp_id == Company.comp_id",
        foreign_keys="Outbound.outb_comp_id"
    )

    @staticmethod
    def head_extension(comp_id, outbounds):
        ext = f"[{comp_id}-outbound]\n"
        for outb in outbounds:
            ext += f'include => {comp_id}-outbound-{outb.outb_id} ; route: {outb.outb_route_name}\n'

        return f'{ext}\n'

    def extension_body(self, comp_id):
        ext = f"[{comp_id}-outbound-{self.outb_id}] ; route: {self.outb_route_name}\n"

        for dp in self.dialpatterns:
            if not (pattern := f'{dp.dp_prefix}{dp.dp_match_pattern}').isdigit():
                pattern = f"_{pattern}"
            ext += f'exten => {pattern},1,NoOp(blablacar)\n'
            ext += f'exten => {pattern},n,Set(HASH(USEROUTCID)=${{ODBC_USEROUTCID({comp_id}-${{CALLEID(num)}})}})\n'
            ext += f'exten => {pattern},n,NoOp(USER_OUT_CID is: ${{HASH(USEROUTCID,user_out_cid)}})\n'

            for trseq in self.trunksequences:
                if self.outb_override_extension == 'Y' and self.outb_route_cid != '':
                    ext += f'exten => {pattern},n,ExecIf($["${{HASH(USEROUTCID,user_out_cid)}}" != ""]' \
                           f'?Set(XBEE-OUTBOUND-CID={self.outb_route_cid})' \
                           f':Set(XBEE-OUTBOUND-CID=${{HASH(USEROUTCID,user_out_cid)}}))\n'
                else:
                    ext += f'exten => {pattern},n,Set(XBEE-OUTBOUND-CID={self.outb_route_cid})\n'
                ext += f'exten => {pattern},n,Set(XBEE-DIAL-OPTIONS=)\n'
                # TODO da rivedere
                ext += f'exten => {pattern},n,' \
                       f'Macro(dial-out,SIP/TRUNK2/0039${{EXTEN:2}},60,' \
                       f'${{XBEE-DIAL-OPTIONS}},${{XBEE-OUTBOUND-CID}},2)\n\n'

        ext += 'exten => h,1,Hangup\n\n'

        return ext


class DialPattern(conn.Model):
    __table__ = Table(
        'xbee_dialpattern',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    outbound = relationship(
        "Outbound",
        backref='dialpatterns',
        primaryjoin="DialPattern.dp_outbound == Outbound.outb_id",
        foreign_keys="DialPattern.dp_outbound"
    )


class TrunkSequenceMatchedRoute(conn.Model):
    __table__ = Table(
        'xbee_trunksequencematchedroute',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    outbound = relationship(
        "Outbound",
        backref='trunksequences',
        primaryjoin="TrunkSequenceMatchedRoute.tsmr_outbound == Outbound.outb_id",
        foreign_keys="TrunkSequenceMatchedRoute.tsmr_outbound"
    )
    trunk = relationship(
        "Trunk",
        backref='trunksequence',
        primaryjoin="TrunkSequenceMatchedRoute.tsmr_trunk == Trunk.tru_comp_id",
        foreign_keys="TrunkSequenceMatchedRoute.tsmr_trunk"
    )


class Trunk(conn.Model):
    __table__ = Table(
        'xbee_trunk',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


class CallSwitchCondition(conn.Model):
    __table__ = Table(
        'xbee_callswitchcondition',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='callswitchconditions',
        primaryjoin="CallSwitchCondition.csc_comp_id == Company.comp_id",
        foreign_keys="CallSwitchCondition.csc_comp_id"
    )

    def extension_body(self, comp_id):
        ext = f'[{comp_id}-cfc-{self.csc_call_flow_toggle_feature_code_index}]\n'
        ext += f'exten => s,1,Set(HASH(CFC)=${{ODBC_CFC({comp_id},{self.csc_call_flow_toggle_feature_code_index})}})\n'
        ext += f'exten => s,n,GotoIf($["${{HASH(CFC,callfc_active_mode)}}" = "0"]?' \
               f'${{HASH(CFC,callfc_dest_disabled_mode)}}:${{HASH(CFC,callfc_dest_enabled_mode)}})\n'
        # TODO al posto delle variabili callfc_dest_disabled_mode e callfc_dest_enabled_mode inserisci la destinazione

        return ext


class Extension(conn.Model):
    __table__ = Table(
        'xbee_extension',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    company = relationship(
        "Company",
        backref='entensions',
        primaryjoin="Extension.ext_comp_id == Company.comp_id",
        foreign_keys="Extension.ext_comp_id"
    )


class FollowMe(conn.Model):
    __table__ = Table(
        'xbee_followme',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )
    extension = relationship(
        "Extension",
        backref='follome',
        primaryjoin="FollowMe.fme_ext_id == Extension.id",
        foreign_keys="FollowMe.fme_ext_id"
    )
    music_on_hold = relationship(
        "MusicOnHoldCategory",
        backref='follome',
        primaryjoin="FollowMe.fme_moh_category == MusicOnHoldCategory.id",
        foreign_keys="FollowMe.fme_moh_category"
    )


class ContentType(conn.Model):
    __table__ = Table(
        'django_content_type',
        MetaData(),
        autoload=True,
        extend_existing=True,
        autoload_with=conn.engine
    )


def get_generic_object(session, django_content_type_id, object_id,
                       xbee_default_prefix=True, custom_prefix=None, id_name_content_type='id',
                       id_name_object='id'):
    result_model = session.query(ContentType)\
        .filter(
            text(f"{ContentType.__table__.name}.{id_name_content_type}={django_content_type_id}"),
        )

    table_name = result_model[0].model
    if xbee_default_prefix:
        table_name = f'xbee_{table_name}'
    else:
        table_name = f'{custom_prefix}{table_name}'

    for c in conn.Model._decl_class_registry.values():
        try:
            if c.__table__.name == table_name:
                result = session.query(c)\
                    .filter(
                        text(f"{table_name}.{id_name_object}={object_id}"),
                    )

                return result[0]
        except:
            pass

    return
