#!/usr/bin/python3.8
import os
import sys

from asterisk.agi import *
from sqlalchemy import text
from sqlalchemy.orm import sessionmaker

# import models module
sys.path.append('/opt/xbee/config_script/')

import models_definition as models


# todo organizza in un modulo a parte
def clean_and_exit(session=None):
    if session:
        session.close()
    sys.exit()


def is_internal_call(session, comp_id, number):
    internal_call_result = session.query(models.Extension) \
        .filter(
        text(f"{models.Extension.__table__.name}.ext_number={number}"),
        text(f"{models.Extension.__table__.name}.ext_comp_id={comp_id}")
    )

    return internal_call_result.count() > 0, internal_call_result


# save sys params
company_id = sys.argv[1]  # 00000000003
int_company_id = int(company_id)  # 3
extension_number = sys.argv[2]
xbee_dial = ''

agi = AGI()

Session = sessionmaker(bind=models.conn.engine)
asession = Session()

# todo quando ci sarà il visionid c'è da modificare questa parte di codice
extension = asession.query(models.Extension)\
    .filter(
        text(f"{models.Extension.__table__.name}.ext_number={extension_number}"),
        text(f"{models.Extension.__table__.name}.ext_comp_id={int_company_id}")
    )

if extension.count() > 1:
    agi.verbose('error: somethings goes wrong.')
    agi.hangup()
    clean_and_exit(asession)
else:
    aextension = extension[0]

if aextension.ext_dnd == 1:
    agi.verbose('dnd')
    agi.hangup()
    clean_and_exit(asession)

if aextension.ext_cfu is not None:
    agi.verbose('cnf')
    cfu = aextension.ext_cfu
    is_intern_call, _ = is_internal_call(asession, int_company_id, cfu)

    if is_intern_call:
        xbee_dial = f'SIP/{company_id}-{cfu}'
    else:
        xbee_dial = f'Local/{cfu}@{aextension.ext_context}/n'

    xbee_dial += f',{aextension.ext_ring_time}'

    agi.set_variable('XBEE-DIAL', xbee_dial)
    agi.set_variable('XBEE-RECORDING', aextension.ext_call_recording)
    clean_and_exit(asession)

# follow me check

# XBEE-CALLTYPE o è vuota (follome internal) o inbound (follow me external)
xbee_call_type = 1 if agi.get_variable('XBEE-CALLTYPE') == 'inbound' else 0
agi.verbose(f'{xbee_call_type=}')
agi.verbose('followme')
for fme in aextension.follome:
    if fme.fme_enable == 1 and fme.fme_phone_numbers and fme.fme_type == xbee_call_type:
        for phone_num in fme.fme_phone_numbers.split('\n'):
            is_intern_call, intern_call = is_internal_call(asession, int_company_id, phone_num)
            if is_intern_call:
                if intern_call[0].ext_dnd == 0:
                    xbee_dial += f'SIP/{company_id}-{phone_num.strip()}&'
            else:
                xbee_dial += f'Local/{phone_num.strip()}@{aextension.ext_context}/n&'

        if len(xbee_dial) > 0:
            agi.set_variable('XBEE-PREPENDCID', fme.fme_prepend_cid)

            xbee_dial = xbee_dial[:-1]  # rimuovo l'ultima &
            xbee_dial += ','
            xbee_dial += str(fme.fme_ring_time) if fme.fme_ring_time else str(aextension.ext_ring_time)
            agi.verbose(f'{xbee_dial=}')

if len(xbee_dial) == 0:
    xbee_dial = f'SIP/{company_id}-{extension_number},{aextension.ext_ring_time}'

# TODO imposta la destination no answer e la destination busy
# XBEE-DEST-NOANSWER
# XBEE-DEST-BUSY

agi.set_variable('XBEE-DIAL', xbee_dial)
agi.set_variable('XBEE-RECORDING', aextension.ext_call_recording)
clean_and_exit(asession)
