from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_company.models import Company
from xbee_ivr.forms import IVRCreateForm, IVREditForm, DigitFormset
from xbee_ivr.models import IVR, Digit


class IvrsView(LoginRequiredMixin, BaseClassView):
    """
        List all ivr and allows to create a new one
    """

    template_name = 'xbee_ivr/ivr_list.html'
    redirect_url = 'xbee_ivr:ivr_list'

    def post(self, request):
        form = IVRCreateForm(request.POST)
        if form.is_valid():
            form.save()
            form.save_special_digit()

            messages.success(request, _("IVR created!"))
            return redirect(form.instance)
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        ivrs = IVR.objects.filter(company__in=comps)
        form_ivr = IVRCreateForm(initial={'companies': comps})

        return render(request, self.template_name, {'form_ivr': form_ivr, 'ivrs': ivrs})


class IvrDetailView(LoginRequiredMixin, BaseClassView):
    """
        Detail of an ivr, this view allows to edit the details of the IVR and create it's digits
    """

    template_name = 'xbee_ivr/ivr_detail.html'

    def post(self, request, id_ivr):
        ivr = get_object_or_404(IVR, pk=id_ivr)
        if 'ivr_detail_submitted' in request.POST:
            form = IVREditForm(request.POST, instance=ivr)
            if form.is_valid():
                form.save()
                messages.success(request, _("IVR updated!"))
                return redirect(form.instance)
            else:
                messages.error(request, _("Somethings goes wrong!"))

        elif 'fs_digits_submitted' in request.POST:
            formset = DigitFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    dig = form.save(commit=False)
                    dig.ivr = ivr
                    if (mod := form.cleaned_data.get('module', None)) and \
                            (submod := form.cleaned_data.get('sub_module', None)):
                        model_class_module = mod.model_object.model_class()
                        dig.content_object = model_class_module.objects.get(pk=submod)
                        dig.save()
                        messages.success(request, _(f"Digit created: {dig.digits_pressed}"))

        return redirect(ivr)

    def get(self, request, id_ivr):
        ivr = get_object_or_404(IVR, pk=id_ivr)
        comps = Company.objects.filter(pk=ivr.company.pk)
        form_ivr_detail = IVREditForm(instance=ivr, initial={'companies': comps})
        dg_formset = DigitFormset(form_kwargs={'initial': {'companies': comps}})

        return render(request, self.template_name, {
            'ivr': ivr,
            'form_ivr_detail': form_ivr_detail,
            'dg_formset': dg_formset
        })


class DeleteIvrView(LoginRequiredMixin, BaseClassView):
    """
        delete ivr by id
    """

    redirect_url = 'xbee_ivr:ivr_list'

    def get(self, request, id_ivr):
        IVR.objects.get(pk=id_ivr).delete()
        messages.success(request, _("Deleted!"))

        return redirect(self.redirect_url)


class DeleteDigitView(LoginRequiredMixin, BaseClassView):
    """
        delete digit by id
    """

    def get(self, request, id_digit):
        dig = Digit.objects.get(pk=id_digit)
        ivr = dig.ivr
        dig.delete()

        messages.success(request, _("Deleted!"))
        return redirect(ivr)
