from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse


class IVR(models.Model):
    name = models.CharField(max_length=120, db_column='ivr_name')
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                db_column='ivr_company_id')
    description = models.TextField(db_column='ivr_description')
    announcement = models.ForeignKey('xbee_announcements.Announcement', on_delete=models.CASCADE,
                                     db_column='ivr_announcement_id')
    is_enabled_direct_dial = models.BooleanField(default=True, db_column='ivr_is_enabled_direct_dial')

    timeout = models.PositiveIntegerField(default=0, db_column='ivr_timeout')
    invalid_retries = models.PositiveIntegerField(default=0, db_column='ivr_invalid_retries')
    invalid_retry_recording = models.ForeignKey('xbee_recordings.SystemRecording', default=None, null=True,
                                                on_delete=models.PROTECT, related_name='+',
                                                db_column='ivr_invalid_retry_recording_id')
    append_announcement_to_invalid = models.BooleanField(default=False, db_column='ivr_append_announcement_to_invalid')

    timeout_retries = models.PositiveIntegerField(default=0, db_column='ivr_timeout_retries')
    timeout_retry_recording = models.ForeignKey('xbee_recordings.SystemRecording', default=None, null=True,
                                                on_delete=models.PROTECT, related_name='+',
                                                db_column='ivr_timeout_retry_recording_id')
    append_announcement_to_timeout = models.BooleanField(default=False, db_column='ivr_append_announcement_to_timeout')

    timeout_recording = models.ForeignKey('xbee_recordings.SystemRecording', default=None, null=True, related_name='+',
                                          on_delete=models.PROTECT, db_column='ivr_timeout_recording_id')
    invalid_recording = models.ForeignKey('xbee_recordings.SystemRecording', default=None, null=True, related_name='+',
                                          on_delete=models.PROTECT, db_column='ivr_invalid_recording_id')
    # timeout_destination and invalid_destination are special digits

    def get_absolute_url(self):
        return reverse("xbee_ivr:ivr_detail", kwargs={"id_ivr": self.pk})

    def __str__(self):
        return f'{self.name} - [{self.company.name}]'

    class Meta:
        verbose_name = 'IVR'
        verbose_name_plural = 'IVRs'
        db_table = 'xbee_ivr'


class Digit(models.Model):
    digits_pressed = models.CharField(max_length=20, db_column='dig_digits_pressed')
    ivr = models.ForeignKey(IVR, null=False, on_delete=models.CASCADE, related_name='ivr_digits',
                            db_column='dig_ivr_id')

    # generic foreignkey for attach the modules
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return self.digits_pressed

    class Meta:
        verbose_name = 'Digit'
        verbose_name_plural = 'Digits'
        db_table = 'xbee_digit'
