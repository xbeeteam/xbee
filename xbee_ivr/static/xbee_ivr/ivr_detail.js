$(function () {
    let objTimeout = $('#id_timeout_retries');
    let objInvalid = $('#id_invalid_retries');
    showHideInput(objTimeout, 'timeout');
    showHideInput(objInvalid, 'invalid');

    $(objTimeout).on('change', () => { showHideInput(objTimeout, 'timeout') });
    $(objInvalid).on('change', () => { showHideInput(objInvalid, 'invalid') });
});

function showHideInput(obj, timeoutOrInvalid) {
    let retry = $(`#id_${timeoutOrInvalid}_retry_recording`);
    let announcement = $(`#id_append_announcement_to_${timeoutOrInvalid}`);
    if ($(obj).val() == 0) {
        $(retry).parent().addClass('d-none');
        $(announcement).parent().addClass('d-none');
    } else {
        $(retry).parent().removeClass('d-none');
        $(announcement).parent().removeClass('d-none');
    }
}