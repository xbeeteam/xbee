from django.contrib import admin

from xbee_ivr.models import Digit, IVR

admin.site.register(Digit)
admin.site.register(IVR)
