from django.forms import ModelForm, formset_factory
from django import forms
from django.urls import reverse_lazy

from xbee_announcements.models import Announcement
from xbee_ivr.models import IVR, Digit
from xbee_modules.forms import PluggableModuleModelForm
from xbee_modules.models import Module
from xbee_recordings.models import SystemRecording


class IVREditForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps
                comp_pkk = [compp.pk for compp in comps]

                self.fields['announcement'].queryset = Announcement.objects.filter(company__in=comp_pkk)
                sys_rec_qs = SystemRecording.objects.filter(company__in=comp_pkk)
                self.fields['invalid_retry_recording'].queryset = sys_rec_qs
                self.fields['invalid_recording'].queryset = sys_rec_qs
                self.fields['timeout_retry_recording'].queryset = sys_rec_qs
                self.fields['timeout_recording'].queryset = sys_rec_qs

        # for styling the form template
        self.fields['invalid_retries'].new_group = 'true'
        self.fields['timeout_retries'].new_group = 'true'
        self.fields['timeout_recording'].new_group = 'true'

    class Meta:
        model = IVR
        fields = '__all__'


class IVRCreateForm(IVREditForm):

    module_timeout = forms.ModelChoiceField(queryset=Module.objects.all(), required=False, widget=forms.Select(attrs={
        'id': 'module_select_ajax_timeout', 'url': reverse_lazy('xbee_modules:modules_ajax_list')
    }))
    sub_module_timeout = forms.CharField(required=False, widget=forms.Select(attrs={
        'id': 'sub_module_select_ajax_timeout',
    }))

    module_invalid = forms.ModelChoiceField(queryset=Module.objects.all(), required=False, widget=forms.Select(attrs={
        'id': 'module_select_ajax_invalid', 'url': reverse_lazy('xbee_modules:modules_ajax_list')
    }))
    sub_module_invalid = forms.CharField(required=False, widget=forms.Select(attrs={
        'id': 'sub_module_select_ajax_invalid',
    }))

    def save_special_digit(self):
        """
        Call this method after the save method
        Store into db the special digits timeout_destination and invalid_destination
        """
        if self.cleaned_data['module_timeout'] and self.cleaned_data['sub_module_timeout']:
            model_class_module = self.cleaned_data['module_timeout'].model_object.model_class()
            Digit(
                digits_pressed='t',
                ivr=self.instance,
                content_object=model_class_module.objects.get(pk=self.cleaned_data['sub_module_timeout'])
            ).save()

        if self.cleaned_data['module_invalid'] and self.cleaned_data['sub_module_invalid']:
            model_class_module = self.cleaned_data['module_invalid'].model_object.model_class()
            Digit(
                digits_pressed='i',
                ivr=self.instance,
                content_object=model_class_module.objects.get(pk=self.cleaned_data['sub_module_invalid'])
            ).save()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                comp_pkk = [compp.pk for compp in comps]

                mdl_qs = Module.objects.filter(company_modules__in=comp_pkk).distinct()
                self.fields['module_timeout'].queryset = mdl_qs
                self.fields['module_invalid'].queryset = mdl_qs


class DigitForm(ModelForm):

    module = forms.ModelChoiceField(queryset=Module.objects.all(), required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
    }))
    sub_module = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['module'].queryset = Module.objects.filter(
                    company_modules__in=[compp.pk for compp in comps]
                ).distinct()

        self.fields['module'].new_group = 'true'

    class Meta:
        model = Digit
        fields = '__all__'
        exclude = ('ivr', ) + PluggableModuleModelForm.Meta.exclude


DigitFormset = formset_factory(DigitForm)
