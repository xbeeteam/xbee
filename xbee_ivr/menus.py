from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


Menu.add_item("main", MenuItem(_("IVR"), reverse('xbee_ivr:ivr_list'), weight=500, icon="iconsminds-redirect"))
