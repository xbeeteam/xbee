import sys

from django.apps import AppConfig


class XbeeIvrConfig(AppConfig):
    name = 'xbee_ivr'
    urls = 'ivr'
    context = 'ivr'
    extension = 's'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='IVR')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_ivr.models import IVR

                Module.objects.create(
                    name='IVR',
                    context=self.context,
                    extension=self.extension,
                    model_object=ContentType.objects.get_for_model(IVR)
                ).save()
