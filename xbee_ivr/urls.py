from django.urls import path

from xbee_ivr import views

app_name = 'xbee_ivr'

urlpatterns = [
    path('', views.IvrsView.as_view(), name='ivr_list'),
    path('detail/<int:id_ivr>', views.IvrDetailView.as_view(), name='ivr_detail'),
    path('delete/<int:id_ivr>', views.DeleteIvrView.as_view(), name='delete_ivr'),
    path('digit/delete/<int:id_digit>', views.DeleteDigitView.as_view(), name='delete_digit'),

]
