from django.contrib import admin

from xbee_announcements.models import Announcement

admin.site.register(Announcement)
