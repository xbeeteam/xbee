from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_announcements.forms import AnnouncementForm
from xbee_announcements.models import Announcement


class AnnouncementsView(LoginRequiredMixin, BaseClassView):
    """
        List all announcements and allows to create a new one
    """

    template_name = 'xbee_announcements/announcements_list.html'
    redirect_url = 'xbee_announcements:announcements_list'

    def get(self, request):
        comps = self.get_companies_by_user(request)
        announcements = Announcement.objects.filter(company__in=comps)
        form_announcement = AnnouncementForm(initial={'companies': comps})
        return render(request, self.template_name, {
            'form_announcement': form_announcement,
            'announcements': announcements
        })

    def post(self, request):
        form = AnnouncementForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _("Announcement created!"))
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)


class DeleteAnnouncementView(LoginRequiredMixin, BaseClassView):
    """
        delete announcement by id
    """

    redirect_url = 'xbee_announcements:announcements_list'

    def get(self, request, id_announcement):
        announcement = get_object_or_404(Announcement, pk=id_announcement)
        announcement.delete()
        messages.success(request, _("Deleted!"))
        return redirect(self.redirect_url)
