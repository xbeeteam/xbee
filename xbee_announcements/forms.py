from xbee_announcements.models import Announcement
from xbee_modules.forms import PluggableModuleModelForm
from xbee_recordings.models import SystemRecording


class AnnouncementForm(PluggableModuleModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['recording'].queryset = SystemRecording.objects.filter(company__in=comps)

    class Meta:
        model = Announcement
        fields = '__all__'
        exclude = PluggableModuleModelForm.Meta.exclude
