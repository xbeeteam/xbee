from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _

from xbee_modules.models import PluggableModuleModel


class Announcement(PluggableModuleModel):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_announcements", db_column='ann_comp_id')
    description = models.TextField(db_column='ann_description')
    recording = models.ForeignKey('xbee_recordings.SystemRecording', null=False, on_delete=models.PROTECT,
                                  related_name="+", db_column='ann_recording_id')

    ANSWER_CHANNEL_CHOICES = (
        ('Y', _('Yes')),
        ('N', _('No')),
    )
    answer_channel = models.CharField(default='N', choices=ANSWER_CHANNEL_CHOICES, max_length=3,
                                      db_column='ann_answer_channel')

    def __str__(self):
        return f"{self.company.name} - [{self.description=}]"

    class Meta:
        verbose_name = 'Announcement'
        verbose_name_plural = 'Announcements'
        db_table = 'xbee_announcement'
