import sys

from django.apps import AppConfig


class XbeeAnnouncementsConfig(AppConfig):
    name = 'xbee_announcements'
    urls = 'announcements'
    context = 'announcement'
    extension = 's'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Announcements')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_announcements.models import Announcement

                Module.objects.create(
                    name='Announcements',
                    context=self.context,
                    extension=self.extension,
                    model_object=ContentType.objects.get_for_model(Announcement)
                ).save()
