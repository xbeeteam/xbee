from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


Menu.add_item("main", MenuItem(_("Announcements"), reverse('xbee_announcements:announcements_list'), weight=400,
                               icon="iconsminds-consulting"))
