from django.urls import path

from xbee_announcements import views

app_name = 'xbee_announcements'

urlpatterns = [
    path('', views.AnnouncementsView.as_view(), name='announcements_list'),
    path('delete/<int:id_announcement>', views.DeleteAnnouncementView.as_view(), name='delete_announcement'),

]
