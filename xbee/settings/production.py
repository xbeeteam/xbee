from .base import *

DEBUG = False

ALLOWED_HOSTS = ['msite.com']  # TODO sostituisci l'url del progetto definitivo

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
# TODO configura il database

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
