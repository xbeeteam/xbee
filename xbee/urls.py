"""
xbee URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.apps import apps


urlpatterns = i18n_patterns(
    path('administration/', admin.site.urls),
    path('', include('xbee_account.urls')),

)

for app in settings.INSTALLED_APPS:
    if app.startswith('xbee_'):
        app_config = apps.get_app_config(app.rsplit('.')[0])
        if hasattr(app_config, 'urls') and app_config.urls:
            urlpatterns += i18n_patterns(
                path(f'{app_config.urls}/', include(f'{app_config.name}.urls')),
            )

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar
    urlpatterns = [
        path('debug/', include(debug_toolbar.urls)),
    ] + urlpatterns
