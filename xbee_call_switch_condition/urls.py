from django.urls import path

from xbee_call_switch_condition import views

app_name = 'xbee_call_switch_condition'

urlpatterns = [
    path('', views.CallSwitchConditionsView.as_view(), name='call_switch_list'),
    path('delete/<int:id_csc>', views.DeleteCallSwitchConditionView.as_view(), name='delete_csc'),

]
