from django.contrib import admin

from xbee_call_switch_condition.models import CallSwitchCondition

admin.site.register(CallSwitchCondition)
