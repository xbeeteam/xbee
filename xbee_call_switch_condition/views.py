from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_call_switch_condition.forms import CallSwitchConditionForm
from xbee_call_switch_condition.models import CallSwitchCondition


class CallSwitchConditionsView(LoginRequiredMixin, BaseClassView):
    """
        List all call switch conditions and allows to create a new one
    """

    template_name = 'xbee_call_switch_condition/call_switch_list.html'
    redirect_url = 'xbee_call_switch_condition:call_switch_list'

    def post(self, request):
        form = CallSwitchConditionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _("Call switch condition created!"))
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        form_csc = CallSwitchConditionForm(initial={'companies': comps})
        cscs = CallSwitchCondition.objects.filter(company__in=comps)
        return render(request, self.template_name, {
            'form_csc': form_csc,
            'cscs': cscs,
        })


class DeleteCallSwitchConditionView(LoginRequiredMixin, BaseClassView):
    """
        delete call switch condition by id
    """

    redirect_url = 'xbee_call_switch_condition:call_switch_list'

    def get(self, request, id_csc):
        CallSwitchCondition.objects.get(pk=id_csc).delete()
        messages.success(request, _("Deleted!"))

        return redirect(self.redirect_url)
