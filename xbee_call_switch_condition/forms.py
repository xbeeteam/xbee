from django import forms
from django.forms import ModelForm
from django.urls import reverse_lazy

from xbee_call_switch_condition.models import CallSwitchCondition
from xbee_modules.models import Module


class CallSwitchConditionForm(ModelForm):

    module_normal_flow = forms.ModelChoiceField(
        queryset=Module.objects.all(),
        required=False,
        widget=forms.Select(
            attrs={
                'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
            }
        )
    )
    sub_module_normal_flow = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    module_override_flow = forms.ModelChoiceField(
        queryset=Module.objects.all(),
        required=False,
        widget=forms.Select(
            attrs={
                'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
            }
        )
    )
    sub_module_override_flow = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps
                mdls = Module.objects.filter(
                    company_modules__in=[compp.pk for compp in comps]
                ).distinct()
                self.fields['module_normal_flow'].queryset = mdls
                self.fields['module_override_flow'].queryset = mdls

    def save(self, commit=True):
        csc_obj = super().save(commit=False)
        if self.cleaned_data['module_normal_flow'] and self.cleaned_data['sub_module_normal_flow']:
            model_class_module = self.cleaned_data['module_normal_flow'].model_object.model_class()
            csc_obj.normal_flow = model_class_module.objects.get(
                pk=self.cleaned_data['sub_module_normal_flow']
            )

        if self.cleaned_data['module_override_flow'] and self.cleaned_data['sub_module_override_flow']:
            model_class_module = self.cleaned_data['module_override_flow'].model_object.model_class()
            csc_obj.override_flow = model_class_module.objects.get(
                pk=self.cleaned_data['sub_module_override_flow']
            )

        if commit:
            csc_obj.save()

        return csc_obj

    class Meta:
        model = CallSwitchCondition
        fields = '__all__'
        exclude = ('normal_flow_content_type', 'normal_flow_object_id',
                   'override_flow_content_type', 'override_flow_object_id', )
