import sys

from django.apps import AppConfig


class XbeeCallSwitchConditionConfig(AppConfig):
    name = 'xbee_call_switch_condition'
    urls = 'call-switch-conditions'
    context = 'callswitchconditions'
    extension = 's'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Call Switch Conditions')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_call_switch_condition.models import CallSwitchCondition

                Module.objects.create(
                    name='Call Switch Conditions',
                    context=self.context,
                    extension=self.extension,
                    model_object=ContentType.objects.get_for_model(CallSwitchCondition)
                ).save()
