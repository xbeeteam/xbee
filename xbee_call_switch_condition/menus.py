from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("Call Switch Conditions"), reverse('xbee_call_switch_condition:call_switch_list'),
                               weight=900, icon="iconsminds-switch"))
