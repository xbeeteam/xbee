from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _


class CallSwitchCondition(models.Model):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_callswitchcondition", db_column='csc_comp_id')
    description = models.TextField(db_column='csc_description')
    call_flow_toggle_feature_code_index = models.PositiveIntegerField(
        choices=((i, i) for i in range(20)),
        db_column='csc_call_flow_toggle_feature_code_index'
    )

    CURRENT_MODE_CHOICES = (
        (0, _('Normal')),
        (1, _('Override')),
     )
    current_mode = models.PositiveIntegerField(choices=CURRENT_MODE_CHOICES, db_column='csc_current_mode')

    normal_flow_content_type = models.ForeignKey(ContentType, default=None, null=True,
                                                 related_name='+', on_delete=models.CASCADE)
    normal_flow_object_id = models.PositiveIntegerField(default=0)
    normal_flow = GenericForeignKey('normal_flow_content_type', 'normal_flow_object_id')

    override_flow_content_type = models.ForeignKey(ContentType, default=None, null=True,
                                                   related_name='+', on_delete=models.CASCADE)
    override_flow_object_id = models.PositiveIntegerField(default=0)
    override_flow = GenericForeignKey('override_flow_content_type', 'override_flow_object_id')

    def __str__(self):
        return f"{self.call_flow_toggle_feature_code_index} [{self.company.name}]"

    class Meta:
        verbose_name = 'CallSwitchCondition'
        verbose_name_plural = 'CallSwitchConditions'
        db_table = 'xbee_callswitchcondition'
