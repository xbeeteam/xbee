from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import formset_factory
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_company.models import Company
from xbee_extensions.forms import ExtensionForm, ExtensionEditForm, FollowMeForm
from xbee_extensions.models import Extension, FollowMe


class ExtensionsView(LoginRequiredMixin, BaseClassView):
    """
        List all extensions
    """

    template_name = 'xbee_extensions/extensions_list.html'

    def get(self, request):
        comps = self.get_companies_by_user(request)
        exts = Extension.objects.filter(company__in=comps)
        return render(request, self.template_name, {'extensions': exts})


class ExtensionCreateView(LoginRequiredMixin, BaseClassView):
    """
        Create new estensions
    """

    template_name = 'xbee_extensions/extension_create.html'
    redirect_url = 'xbee_extensions:extensions_list'

    def post(self, request):
        if request.is_ajax():
            first_ext = int(request.POST.get('first_ext'))
            last_ext = int(request.POST.get('last_ext'))
            num_extra = last_ext - first_ext + 1

            ExtensionFormset = formset_factory(ExtensionForm, extra=num_extra)
            comps = self.get_companies_by_user(request)
            ext_formset = ExtensionFormset(form_kwargs={
                'initial': {
                    'companies': comps,
                    'secret': get_random_string(length=20)
                }
            })
            for idx, frm in enumerate(ext_formset):
                frm.fields['number'].initial = first_ext + idx

            context = {'ext_formset': ext_formset}
            html_template = render_to_string("xbee_extensions/extension_formset.html", context, request=request)
            return HttpResponse(html_template)

        ExtensionFormset = formset_factory(ExtensionForm)
        ext_formset = ExtensionFormset(request.POST)
        if ext_formset.is_valid():
            for frm in ext_formset:
                if frm.is_valid():
                    extens = frm.save(commit=False)
                    comp_id = str(extens.company.pk).zfill(11)  # TODO ricontrolla quando attivi vision

                    extens.name = f'{comp_id}-{extens.number}'
                    extens.context = f'{comp_id}-cos-general'
                    extens.caller_id = extens.number
                    extens.parameters = {
                        'host': 'dynamic',
                        'type': 'friend',
                        'nat': 'yes',
                        'busylevel': 1,
                        'qualify': 'no',
                        'permit': '', 'deny': '', 'transport': '', 'dtmfmode': '', 'directmedia': '',
                        'defaultuser': '', 'callgroup': '', 'pickupgroup': '', 'language': '', 'allow': '',
                        'disallow': '', 'insecure': '', 'trustrpid': '', 'progressinband': '', 'accountcode': '',
                        'callerid': '', 'callcounter': '', 'videosupport': '', 'mailbox': '', 'session_timers': '',
                        'session_expires': 3600, 'session_minse': None, 'session_refresher': '',
                        't38pt_usertpsource': '', 'qualifyfreq': None, 'faxdetect': '', 'fullname': '',
                        'cid_number': '', 'call_limit': None
                    }
                    extens.save()

        return redirect(self.redirect_url)

    def get(self, request):
        return render(request, self.template_name)


class ExtensionDetailView(LoginRequiredMixin, BaseClassView):

    template_name = 'xbee_extensions/extension_detail.html'

    def post(self, request, id_ext):
        ext = get_object_or_404(Extension, pk=id_ext)

        if 'ext_detail_submitted' in request.POST:
            form = ExtensionEditForm(request.POST, instance=ext)
            if form.is_valid():
                form.dump()
                form.save()
                messages.success(request, _("Extension updated!"))
        else:
            form = FollowMeForm(request.POST)
            if form.is_valid():
                fwme = form.save(commit=False)
                fwme.extension = ext
                if 'followme_0_submitted' in request.POST:
                    fwme.type = 0
                else:
                    fwme.type = 1

                fwme.save()
                messages.success(request, _("Followme added!"))

        return redirect(ext)

    def get(self, request, id_ext):
        ext = get_object_or_404(Extension, pk=id_ext)
        comps = Company.objects.filter(pk=ext.company.pk)
        form_ext = ExtensionEditForm(instance=ext, initial={'companies': comps})
        form_ext.load()

        followme_internal = FollowMe.objects.filter(extension=ext, type=0)
        followme_external = FollowMe.objects.filter(extension=ext, type=1)
        form_followme = FollowMeForm(initial={'music_on_holding': comps.first().company_music_categories.all()})

        return render(request, self.template_name, {
            'ext': ext,
            'form_ext': form_ext,
            'followme_internal': followme_internal,
            'followme_external': followme_external,
            'form_followme': form_followme,
        })


class DeleteFollowMeView(LoginRequiredMixin, BaseClassView):
    def get(self, request, id_followme):
        fwme = get_object_or_404(FollowMe, pk=id_followme)
        ext = fwme.extension
        fwme.delete()
        messages.success(request, _("Followme deleted!"))

        return redirect(ext)


class DisableFollowMeView(LoginRequiredMixin, BaseClassView):
    def get(self, request, id_followme):
        fwme = get_object_or_404(FollowMe, pk=id_followme)
        if fwme.enable == 1:
            fwme.enable = 0
            messages.success(request, _("Followme disabled!"))
        else:
            fwme.enable = 1
            messages.success(request, _("Followme enabled!"))
        fwme.save()

        return redirect(fwme.extension)
