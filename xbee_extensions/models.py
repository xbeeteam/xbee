from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.urls import reverse

from django.utils.translation import ugettext_lazy as _


class Extension(models.Model):
    YES_NO_CHOICES = (
        ('yes', _('yes')),
        ('no', _('no')),
    )

    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_extensions", db_column='ext_comp_id')
    number = models.PositiveIntegerField(db_column='ext_number')
    name = models.CharField(max_length=40, db_column='ext_name')
    secret = models.CharField(max_length=40, db_column='ext_secret')
    context = models.CharField(max_length=40, db_column='ext_context')
    ring_time = models.SmallIntegerField(default=30, db_column='ext_ring_time')

    dnd = models.PositiveIntegerField(default=0, db_column='ext_dnd')  # 0 False, 1 True
    cfu = models.CharField(max_length=50, null=True, default=None, db_column='ext_cfu')

    CALL_RECORDING_CHOICES = (
        ('yes', _('yes')),
        ('no', _('no')),
        ('on demand', _('on demand')),
    )
    call_recording = models.CharField(choices=CALL_RECORDING_CHOICES, max_length=10, db_column='ext_call_recording')
    mario = models.CharField(choices=YES_NO_CHOICES, max_length=3, db_column='ext_mario')
    user_group = models.PositiveIntegerField(db_column='ext_user_group')
    caller_id = models.CharField(max_length=40, db_column='ext_caller_id')

    TECHNOLOGY_CHOICES = (
        ('sip', 'sip'),
        ('pjsip', 'pjsip'),
        ('iax2', 'iax2'),
    )
    technology = models.CharField(choices=TECHNOLOGY_CHOICES, max_length=5, db_column='ext_technology')
    parameters = JSONField(db_column='ext_parameters')

    def get_absolute_url(self):
        return reverse("xbee_extensions:extension_detail", kwargs={"id_ext": self.pk})

    class Meta:
        verbose_name = 'Extension'
        verbose_name_plural = 'Extensions'
        db_table = 'xbee_extension'


class FollowMe(models.Model):
    extension = models.ForeignKey('Extension', null=False, on_delete=models.PROTECT,
                                  related_name="extension_follow_me", db_column='fme_ext_id')
    enable = models.SmallIntegerField(db_column='fme_enable')
    type = models.SmallIntegerField(db_column='fme_type')
    ring_time = models.SmallIntegerField(default=30, db_column='fme_ring_time')
    phone_numbers = models.TextField(db_column='fme_phone_numbers')
    music_on_holding = models.ForeignKey('xbee_recordings.MusicOnHoldingCategory', null=False,
                                         on_delete=models.PROTECT, related_name="+", db_column='fme_moh_category')

    prepend_cid = models.CharField(default=None, null=True, blank=True, max_length=50, db_column='fme_prepend_cid')

    destination_busy_content_type = models.ForeignKey(ContentType, default=None, null=True,
                                                      related_name='+', on_delete=models.CASCADE)
    destination_busy_object_id = models.PositiveIntegerField(default=0)
    destination_busy = GenericForeignKey('destination_busy_content_type', 'destination_busy_object_id')

    destination_no_answer_content_type = models.ForeignKey(ContentType, default=None, null=True,
                                                           related_name='+', on_delete=models.CASCADE)
    destination_no_answer_object_id = models.PositiveIntegerField(default=0)
    destination_no_answer = GenericForeignKey('destination_no_answer_content_type', 'destination_no_answer_object_id')

    class Meta:
        verbose_name = 'FollowMe'
        verbose_name_plural = 'FollowMe'
        db_table = 'xbee_followme'


class SipFriends(models.Model):
    YES_NO_CHOICES = (
        ('yes', _('yes')),
        ('no', _('no')),
    )

    name = models.CharField(max_length=50, unique=True)
    ipaddr = models.CharField(max_length=15, null=True, blank=True)
    port = models.PositiveIntegerField(null=True, blank=True)
    regseconds = models.PositiveIntegerField(null=True, blank=True)
    defaultuser = models.CharField(max_length=30, null=True, blank=True)
    fullcontact = models.CharField(max_length=128, null=True, blank=True)
    regserver = models.CharField(max_length=20, null=True, blank=True)
    useragent = models.CharField(max_length=20, null=True, blank=True)
    lastms = models.PositiveIntegerField(null=True, blank=True)
    host = models.CharField(max_length=40, null=True, blank=True)

    TYPE_CHOICES = (
        ('friend', 'friend'),
        ('user', 'user'),
        ('peer', 'peer'),
    )
    type = models.CharField(choices=TYPE_CHOICES, max_length=6, null=True, blank=True)
    context = models.CharField(max_length=40, null=True, blank=True)
    permit = models.CharField(max_length=40, null=True, blank=True)
    deny = models.CharField(max_length=40, null=True, blank=True)
    secret = models.CharField(max_length=40, null=True, blank=True)
    md5secret = models.CharField(max_length=40, null=True, blank=True)
    remotesecret = models.CharField(max_length=40, null=True, blank=True)

    TRASPORT_CHOICES = (
        ('udp', 'udp'),
        ('tcp', 'tcp'),
        ('udp,tcp', 'udp,tcp'),
        ('tcp,udp', 'tcp,udp'),
    )
    transport = models.CharField(choices=TRASPORT_CHOICES, max_length=7, null=True, blank=True)

    DTMFMODE_CHOICES = (
        ('rfc2833', 'rfc2833'),
        ('info', 'info'),
        ('shortinfo', 'shortinfo'),
        ('inband', 'inband'),
        ('auto', 'auto'),
    )
    dtmfmode = models.CharField(choices=DTMFMODE_CHOICES, max_length=9, null=True, blank=True)

    DIRECTMEDIA_CHOICES = (
        ('yes', 'yes'),
        ('no', 'no'),
        ('nonat', 'nonat'),
        ('update', 'update'),
    )
    directmedia = models.CharField(choices=DIRECTMEDIA_CHOICES, max_length=6, null=True, blank=True)

    NAT_CHOICES = (
        ('yes', 'yes'),
        ('no', 'no'),
        ('never', 'never'),
        ('route', 'route'),
    )
    nat = models.CharField(choices=NAT_CHOICES, max_length=5, null=True, blank=True)
    callgroup = models.CharField(max_length=40, null=True, blank=True)
    pickupgroup = models.CharField(max_length=40, null=True, blank=True)
    language = models.CharField(max_length=40, null=True, blank=True)
    allow = models.CharField(max_length=40, null=True, blank=True)
    disallow = models.CharField(max_length=40, null=True, blank=True)
    insecure = models.CharField(max_length=40, null=True, blank=True)
    trustrpid = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)

    PROGRESSINBAND_CHOICES = (
        ('yes', 'yes'),
        ('no', 'no'),
        ('never', 'never'),
    )
    progressinband = models.CharField(choices=PROGRESSINBAND_CHOICES, max_length=5, null=True, blank=True)
    promiscredir = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    useclientcode = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    accountcode = models.CharField(max_length=40, null=True, blank=True)
    setvar = models.CharField(max_length=40, null=True, blank=True)
    callerid = models.CharField(max_length=40, null=True, blank=True)
    amaflags = models.CharField(max_length=40, null=True, blank=True)
    callcounter = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    busylevel = models.PositiveIntegerField(null=True, blank=True)
    allowoverlap = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    allowsubscribe = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    videosupport = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    maxcallbitrate = models.PositiveIntegerField(null=True, blank=True)
    rfc2833compensate = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    mailbox = models.CharField(max_length=40, null=True, blank=True)

    SESSION_TIMERS_CHOICES = (
        ('accept', 'accept'),
        ('refuse', 'refuse'),
        ('originate', 'originate'),
    )
    session_timers = models.CharField(choices=SESSION_TIMERS_CHOICES, max_length=9, null=True, blank=True,
                                      db_column='session-timers')
    session_expires = models.PositiveIntegerField(null=True, blank=True, db_column='session-expires')
    session_minse = models.PositiveIntegerField(null=True, blank=True, db_column='session-minse')

    SESSION_REFRESHER_CHOICES = (
        ('uac', 'uac'),
        ('uas', 'uas'),
    )
    session_refresher = models.CharField(choices=SESSION_REFRESHER_CHOICES, max_length=3, null=True, blank=True,
                                         db_column='session-refresher')
    t38pt_usertpsource = models.CharField(max_length=40, null=True, blank=True)
    regexten = models.CharField(max_length=40, null=True, blank=True)
    fromdomain = models.CharField(max_length=40, null=True, blank=True)
    fromuser = models.CharField(max_length=40, null=True, blank=True)
    qualify = models.CharField(max_length=40, null=True, blank=True)
    defaultip = models.CharField(max_length=40, null=True, blank=True)
    rtptimeout = models.PositiveIntegerField(null=True, blank=True)
    rtpholdtimeout = models.PositiveIntegerField(null=True, blank=True)
    sendrpid = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    outboundproxy = models.CharField(max_length=40, null=True, blank=True)
    callbackextension = models.CharField(max_length=40, null=True, blank=True)
    registertrying = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    timert1 = models.PositiveIntegerField(null=True, blank=True)
    timerb = models.PositiveIntegerField(null=True, blank=True)
    qualifyfreq = models.PositiveIntegerField(null=True, blank=True)
    constantssrc = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    contactpermit = models.CharField(max_length=40, null=True, blank=True)
    contactdeny = models.CharField(max_length=40, null=True, blank=True)
    usereqphone = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    textsupport = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    faxdetect = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    buggymwi = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    auth = models.CharField(max_length=40, null=True, blank=True)
    fullname = models.CharField(max_length=40, null=True, blank=True)
    trunkname = models.CharField(max_length=40, null=True, blank=True)
    cid_number = models.CharField(max_length=40, null=True, blank=True)

    CALLINGPRES_CHOICES = (
        ('allowed_not_screened', 'allowed_not_screened'),
        ('allowed_passed_screen', 'allowed_passed_screen'),
        ('allowed_failed_screen', 'allowed_failed_screen'),
        ('allowed', 'allowed'),
        ('prohib_not_screened', 'prohib_not_screened'),
        ('prohib_passed_screen', 'prohib_passed_screen'),
        ('prohib_failed_screen', 'prohib_failed_screen'),
        ('prohib', 'prohib'),
    )
    callingpres = models.CharField(choices=CALLINGPRES_CHOICES, max_length=22, null=True, blank=True)
    mohinterpret = models.CharField(max_length=40, null=True, blank=True)
    mohsuggest = models.CharField(max_length=40, null=True, blank=True)
    parkinglot = models.CharField(max_length=40, null=True, blank=True)
    hasvoicemail = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    subscribemwi = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    vmexten = models.CharField(max_length=40, null=True, blank=True)
    autoframing = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    rtpkeepalive = models.PositiveIntegerField(null=True, blank=True)
    call_limit = models.PositiveIntegerField(null=True, blank=True, db_column='call-limit')
    g726nonstandard = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    ignoresdpversion = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    allowtransfer = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)
    dynamic = models.CharField(choices=YES_NO_CHOICES, max_length=3, null=True, blank=True)

    class Meta:
        verbose_name = 'SipFriend'
        verbose_name_plural = 'SipFriends'
        db_table = 'xbee_sipfriends'
