import sys

from django.apps import AppConfig


class XbeeExtensionsConfig(AppConfig):
    name = 'xbee_extensions'
    urls = 'extensions'
    context = 'users'

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Users')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_extensions.models import Extension

                Module.objects.create(
                    name='Users',
                    context=f'{self.context}',
                    model_object=ContentType.objects.get_for_model(Extension)
                ).save()
