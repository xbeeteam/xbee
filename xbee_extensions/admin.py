from django.contrib import admin

from xbee_extensions.models import SipFriends, Extension

admin.site.register(SipFriends)
admin.site.register(Extension)
