from django.urls import path

from xbee_extensions import views

app_name = 'xbee_extensions'

urlpatterns = [
    path('', views.ExtensionsView.as_view(), name='extensions_list'),
    path('create/', views.ExtensionCreateView.as_view(), name='extension_create'),
    path('detail/<int:id_ext>', views.ExtensionDetailView.as_view(), name='extension_detail'),
    path('followme/delete/<int:id_followme>', views.DeleteFollowMeView.as_view(), name='delete_follow_me'),
    path('followme/disable/<int:id_followme>', views.DisableFollowMeView.as_view(), name='disable_follow_me'),

]
