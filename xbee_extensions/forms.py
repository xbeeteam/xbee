from django import forms
from django.forms import ModelForm
from django.urls import reverse_lazy

from xbee_extensions.models import Extension, FollowMe, SipFriends
from xbee_modules.models import Module


class ExtensionForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            self.fields['secret'].queryset = initial.get('secret', '')
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps

    class Meta:
        model = Extension
        fields = '__all__'
        exclude = ('parameters', 'name', 'context', 'caller_id', 'ring_time')


class ExtensionEditForm(ModelForm):
    host = forms.CharField(required=False)
    type = forms.ChoiceField(required=False, choices=SipFriends.TYPE_CHOICES)
    nat = forms.ChoiceField(required=False, choices=SipFriends.NAT_CHOICES)
    busylevel = forms.IntegerField(required=False)
    qualify = forms.CharField(required=False)
    permit = forms.CharField(required=False)
    deny = forms.CharField(required=False)
    transport = forms.ChoiceField(required=False, choices=SipFriends.TRASPORT_CHOICES)
    dtmfmode = forms.ChoiceField(required=False, choices=SipFriends.DTMFMODE_CHOICES)
    directmedia = forms.ChoiceField(required=False, choices=SipFriends.DIRECTMEDIA_CHOICES)
    defaultuser = forms.CharField(required=False)
    callgroup = forms.CharField(required=False)
    pickupgroup = forms.CharField(required=False)
    language = forms.CharField(required=False)
    allow = forms.CharField(required=False)
    disallow = forms.CharField(required=False)
    insecure = forms.CharField(required=False)
    trustrpid = forms.ChoiceField(required=False, choices=SipFriends.YES_NO_CHOICES)
    progressinband = forms.ChoiceField(required=False, choices=SipFriends.PROGRESSINBAND_CHOICES)
    accountcode = forms.CharField(required=False)
    callerid = forms.CharField(required=False)
    callcounter = forms.ChoiceField(required=False, choices=SipFriends.YES_NO_CHOICES)
    videosupport = forms.ChoiceField(required=False, choices=SipFriends.YES_NO_CHOICES)
    mailbox = forms.CharField(required=False)
    session_timers = forms.ChoiceField(required=False, choices=SipFriends.SESSION_TIMERS_CHOICES)
    session_expires = forms.IntegerField(required=False)
    session_minse = forms.IntegerField(required=False)
    session_refresher = forms.ChoiceField(required=False, choices=SipFriends.SESSION_REFRESHER_CHOICES)
    t38pt_usertpsource = forms.CharField(required=False)
    qualifyfreq = forms.IntegerField(required=False)
    faxdetect = forms.ChoiceField(required=False, choices=SipFriends.YES_NO_CHOICES)
    fullname = forms.CharField(required=False)
    cid_number = forms.CharField(required=False)
    call_limit = forms.IntegerField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps

    def load(self):
        """
            Call this method after initialization in the view
        """
        for key, value in self.instance.parameters.items():
            self.fields[key].initial = value

    def dump(self):
        """
            Call this method after is_valid() and before save()
        """
        new_parameters = {}
        for key, value in self.instance.parameters.items():
            new_parameters[key] = self.cleaned_data[key]
        self.instance.parameters = new_parameters

    class Meta:
        model = Extension
        fields = '__all__'
        exclude = ('parameters',)


class FollowMeForm(ModelForm):

    module_destination_busy = forms.ModelChoiceField(
        queryset=Module.objects.all(),
        required=False,
        widget=forms.Select(
            attrs={
                'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
            }
        )
    )
    sub_module_destination_busy = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    module_destination_no_answer = forms.ModelChoiceField(
        queryset=Module.objects.all(),
        required=False,
        widget=forms.Select(
            attrs={
                'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
            }
        )
    )
    sub_module_destination_no_answer = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if mohc := initial.get('music_on_holding', False):
                self.fields['music_on_holding'].queryset = mohc
        self.fields['enable'].widget.attrs['min'] = 0
        self.fields['enable'].widget.attrs['max'] = 1

        self.fields['module_destination_busy'].new_group = 'true'
        self.fields['module_destination_no_answer'].new_group = 'true'

    def save(self, commit=True):
        followme_obj = super().save(commit=False)
        if self.cleaned_data['module_destination_busy'] and self.cleaned_data['sub_module_destination_busy']:
            model_class_module = self.cleaned_data['module_destination_busy'].model_object.model_class()
            followme_obj.destination_busy = model_class_module.objects.get(
                pk=self.cleaned_data['sub_module_destination_busy']
            )

        if self.cleaned_data['module_destination_no_answer'] and self.cleaned_data['sub_module_destination_no_answer']:
            model_class_module = self.cleaned_data['module_destination_no_answer'].model_object.model_class()
            followme_obj.destination_no_answer = model_class_module.objects.get(
                pk=self.cleaned_data['sub_module_destination_no_answer']
            )

        if commit:
            followme_obj.save()

        return followme_obj

    class Meta:
        model = FollowMe
        fields = '__all__'
        exclude = ('extension', 'type', 'destination_busy_content_type', 'destination_busy_object_id',
                   'destination_busy', 'destination_no_answer_content_type', 'destination_no_answer_object_id',
                   'destination_no_answer')
