from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


Menu.add_item("main", MenuItem(_("Extension"), reverse('xbee_extensions:extensions_list'), weight=100,
                               icon="iconsminds-factory-1",
                               check=lambda request: request.user.has_perm('xbee_account.xbee_perm_reseller_admin') or
                                    request.user.has_perm('xbee_account.xbee_perm_reseller')))
