from django.urls import path

from xbee_trunk import views

app_name = 'xbee_trunk'

urlpatterns = [
    path('', views.TrunksView.as_view(), name='trunk_list'),
    path('delete/<int:id_trunk>', views.DeleteTrunkView.as_view(), name='delete_trunk'),
]
