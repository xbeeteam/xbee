from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _

from utils.base_class_view import BaseClassView
from xbee_trunk.forms import TrunkForm
from xbee_trunk.models import Trunk


class TrunksView(LoginRequiredMixin, BaseClassView):
    """
        List all trunk and allows to create a new one
    """

    template_name = 'xbee_trunk/trunk_list.html'
    redirect_url = 'xbee_trunk:trunk_list'

    def post(self, request):
        form = TrunkForm(request.POST)
        if form.is_valid():
            form.save()
            form.save_sip_trunks()

            messages.success(request, _("Trunk created!"))
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        trunks = Trunk.objects.filter(company__in=comps)
        form_trunk = TrunkForm(initial={'companies': comps})

        return render(request, self.template_name, {'trunks': trunks, 'form_trunk': form_trunk})


class DeleteTrunkView(LoginRequiredMixin, BaseClassView):
    """
        delete trunk by id
    """

    redirect_url = 'xbee_trunk:trunk_list'

    def get(self, request, id_trunk):
        Trunk.objects.get(pk=id_trunk).delete()

        messages.success(request, _("Deleted!"))
        return redirect(self.redirect_url)
