from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("Trunk"), reverse('xbee_trunk:trunk_list'), weight=900,
                               icon="iconsminds-arrow-barrier"))
