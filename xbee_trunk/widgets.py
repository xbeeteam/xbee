from utils.widgets.base_widget import BaseWidget


class TrunksWidget(BaseWidget):

    def define_context(self):
        self.context['companies'] = self.get_companies_by_user()

    def define_html_path_file(self):
        self.html_file = 'xbee_trunk/widgets/trunks_widget.html'
