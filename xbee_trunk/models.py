from django.db import models
from django.utils.translation import ugettext_lazy as _


class Trunk(models.Model):
    YES_NO_CHOICES = (
        ('Y', _('Yes')),
        ('N', _('No'))
    )

    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name="company_trunks", db_column='tru_comp_id')
    name = models.CharField(max_length=64, db_column='tru_name')
    hide_caller_id = models.CharField(default='N', max_length=2, choices=YES_NO_CHOICES)
    outbound_caller_id = models.CharField(max_length=64, db_column='tru_outbound_caller_id')
    max_channels = models.PositiveIntegerField(default=1, null=False, db_column='tru_max_channels')
    disable_trunk = models.CharField(default='N', max_length=2, choices=YES_NO_CHOICES)

    def __str__(self):
        return f"{self.name} [{self.company.name}]"

    class Meta:
        verbose_name = 'Trunk'
        verbose_name_plural = 'Trunks'
        db_table = 'xbee_trunk'


class SipTrunk(models.Model):
    trunk = models.ForeignKey(Trunk, null=False, on_delete=models.CASCADE, related_name="trunk_sip_trunks",
                              db_column="st_trunk_id")
    og_trunk_name = models.CharField(max_length=64, db_column='st_og_trunk_name')
    og_peer_details = models.TextField(db_column='st_og_peer_details')

    ic_user_context = models.CharField(max_length=64, null=True, db_column='st_ic_user_context')
    ic_user_details = models.TextField(null=True, db_column='st_ic_user_details')
    ic_register_string = models.CharField(max_length=64, null=True, db_column='st_ic_register_string')

    def __str__(self):
        return f"{self.og_trunk_name} [{self.trunk.name}]"

    class Meta:
        verbose_name = 'SipTrunk'
        verbose_name_plural = 'SipTrunks'
        db_table = 'xbee_siptrunk'
