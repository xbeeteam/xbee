from django.contrib import admin

from xbee_trunk.models import Trunk, SipTrunk

admin.site.register(Trunk)
admin.site.register(SipTrunk)
