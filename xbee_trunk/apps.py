import sys

from django.apps import AppConfig

from xbee_trunk.widgets import TrunksWidget


class XbeeTrunkConfig(AppConfig):
    name = 'xbee_trunk'
    urls = 'trunks'
    context = 'trunk'
    extension = 's'
    widgets = [TrunksWidget, ]

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Trunk')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_trunk.models import Trunk

                Module.objects.create(
                    name='Trunk',
                    context=self.context,
                    extension=self.extension,
                    model_object=ContentType.objects.get_for_model(Trunk)
                ).save()
