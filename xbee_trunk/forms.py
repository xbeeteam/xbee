from django import forms
from django.forms import ModelForm

from xbee_trunk.models import Trunk, SipTrunk


class TrunkForm(ModelForm):

    og_trunk_name = forms.CharField()
    og_peer_details = forms.CharField(widget=forms.Textarea)

    ic_user_context = forms.CharField(required=False)
    ic_user_details = forms.CharField(widget=forms.Textarea, required=False)
    ic_register_string = forms.CharField(required=False)

    def save_sip_trunks(self):
        """
        Call this method after the save method
        Store into db the sip trunk information
        """
        if self.cleaned_data['og_trunk_name'] \
                and self.cleaned_data['og_peer_details']:
            SipTrunk(
                trunk=self.instance,
                og_trunk_name=self.cleaned_data['og_trunk_name'],
                og_peer_details=self.cleaned_data['og_peer_details'] if self.cleaned_data['og_peer_details'] else None,
                ic_user_context=self.cleaned_data['ic_user_context'] if self.cleaned_data['ic_user_context'] else None,
                ic_user_details=self.cleaned_data['ic_user_details'] if self.cleaned_data['ic_user_details'] else None,
                ic_register_string=
                self.cleaned_data['ic_register_string'] if self.cleaned_data['ic_register_string'] else None
            ).save()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps

    class Meta:
        model = Trunk
        fields = '__all__'
