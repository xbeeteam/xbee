from django import template

from utils.widgets.dashboard import Dashboard

register = template.Library()


class WidgetsNode(template.Node):

    def render(self, context):
        if 'request' not in context:
            return '<!-- widgets failed to render due to missing request in the context -->'

        Dashboard.set_request(context.request.user)
        context['widgets'] = Dashboard.widgets_instance
        return ''


def generate_apps_widgets(parser, token):
    return WidgetsNode()


register.tag('generate_apps_widgets', generate_apps_widgets)
