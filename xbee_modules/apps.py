from django.apps import AppConfig


class XbeeModulesConfig(AppConfig):
    name = 'xbee_modules'
    urls = 'modules'
