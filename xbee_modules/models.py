from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Module(models.Model):
    id = models.AutoField(primary_key=True, db_column='mod_id')
    name = models.CharField(max_length=50, db_column='mod_name')
    context = models.CharField(max_length=50, db_column='mod_context')
    is_custom_context = models.BooleanField(default=False, db_column='mod_is_custom_context')

    extension = models.CharField(default='exten', null=True, blank=False, max_length=20, db_column='mod_extension')
    model_object = models.ForeignKey(ContentType, null=True, on_delete=models.CASCADE, editable=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Module'
        verbose_name_plural = 'Modules'
        db_table = 'xbee_module'


class PluggableModuleModel(models.Model):
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE)

    # generic foreignkey for attach the modules
    content_type = models.ForeignKey(ContentType, default=None, null=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(default=0)
    content_object = GenericForeignKey()

    class Meta:
        abstract = True
