from django.contrib import admin

from xbee_modules.models import Module

admin.site.register(Module)
