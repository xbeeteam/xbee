from django.urls import path

from xbee_modules import views

app_name = 'xbee_modules'

urlpatterns = [
    path('ajax/', views.ModuleAjaxView.as_view(), name='modules_ajax_list'),

]
