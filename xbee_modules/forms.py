from django.forms import ModelForm
from django import forms
from django.urls import reverse_lazy

from xbee_modules.models import Module


class PluggableModuleModelForm(ModelForm):

    module = forms.ModelChoiceField(queryset=Module.objects.all(), required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax', 'url': reverse_lazy('xbee_modules:modules_ajax_list'),
    }))
    sub_module = forms.CharField(required=False, widget=forms.Select(attrs={
        'my-id': 'module_select_ajax_sub',
    }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps
                self.fields['module'].queryset = Module.objects.filter(
                    company_modules__in=[compp.pk for compp in comps]
                ).distinct()

        self.fields['module'].new_group = 'true'

    def save(self, commit=True):
        pluggable_obj = super().save(commit=False)
        if self.cleaned_data['module'] and self.cleaned_data['sub_module']:
            model_class_module = self.cleaned_data['module'].model_object.model_class()
            pluggable_obj.content_object = model_class_module.objects.get(pk=self.cleaned_data['sub_module'])

        if commit:
            pluggable_obj.save()

        return pluggable_obj

    class Meta:
        exclude = ('content_type', 'object_id', 'content_object')
