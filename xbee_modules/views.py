from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseForbidden
from django.template.loader import render_to_string

from utils.base_class_view import BaseClassView
from xbee_modules.models import Module


class ModuleAjaxView(LoginRequiredMixin, BaseClassView):
    """

    """

    template_name = 'xbee_modules/submodules_option_template.html'

    def post(self, request):
        if module_pk := request.POST.get('module_pk', None):
            model_class_module = Module.objects.get(pk=module_pk).model_object.model_class()
            comps = self.get_companies_by_user(request)

            context = {'sub_modules': model_class_module.objects.filter(company__in=comps)}
            html_content = render_to_string(self.template_name, context)
            return HttpResponse(html_content)
        else:
            return HttpResponseForbidden()
