from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from menu import Menu, MenuItem


# main manu
Menu.add_item("main", MenuItem(_("Routes"), reverse('xbee_routes:routes'), weight=300, icon="iconsminds-phone-3"))
