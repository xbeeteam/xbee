from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404

from utils.base_class_view import BaseClassView
from xbee_company.models import Company
from xbee_recordings.models import MusicOnHoldingCategory
from xbee_routes.forms import InboundForm, OutboundForm, DialPatternFormset, TrunkSequenceMatchedRouteFormset
from xbee_routes.models import Inbound, Outbound, DialPattern, TrunkSequenceMatchedRoute

from django.utils.translation import ugettext_lazy as _

from xbee_trunk.models import Trunk


class RoutesView(LoginRequiredMixin, BaseClassView):
    """
        List all routes and allows to create a new one
    """

    template_name = 'xbee_routes/routes.html'
    redirect_url = 'xbee_routes:routes'

    def post(self, request):
        to_redirect = False
        if 'inbound_submitted' in request.POST:
            form = InboundForm(request.POST)
            msg = _('Inbound route')
        elif 'outbound_submitted' in request.POST:
            form = OutboundForm(request.POST)
            msg = _('Outbound route')
            to_redirect = True

        if form.is_valid():
            form.save()
            messages.success(request, _(f"{msg} created!"))
            if to_redirect:
                return redirect(form.instance)
        else:
            messages.error(request, _("Somethings goes wrong!"))

        return redirect(self.redirect_url)

    def get(self, request):
        comps = self.get_companies_by_user(request)
        mohc = MusicOnHoldingCategory.objects.filter(company__in=comps)
        inbs = Inbound.objects.filter(company__in=comps)
        outs = Outbound.objects.filter(company__in=comps)
        form_inbound = InboundForm(initial={'companies': comps, 'music_on_hold_cats': mohc})
        form_outbound = OutboundForm(initial={'companies': comps, 'music_on_hold_cats': mohc})

        return render(request, self.template_name, {
            'form_inbound': form_inbound,
            'form_outbound': form_outbound,
            'inbounds': inbs,
            'outbounds': outs,
        })


class DeleteRouteView(LoginRequiredMixin, BaseClassView):
    """
        delete route by id
    """

    redirect_url = 'xbee_routes:routes'

    def get(self, request, type, id_route):
        if type == 'inbound':
            Inbound.objects.get(pk=id_route).delete()
        elif type == 'outbound':
            Outbound.objects.get(pk=id_route).delete()
        else:
            messages.error(request, _("Somethings goes wrong!"))
            return redirect(self.redirect_url)

        messages.success(request, _("Deleted!"))
        return redirect(self.redirect_url)


class DeleteDialPatternTrunkSequenceView(LoginRequiredMixin, BaseClassView):
    """
        delete dial pattern or trunk sequance
    """

    redirect_url = 'xbee_routes:routes'

    def get(self, request, type, id_to_delete):
        if type == 'dp':
            obj = DialPattern.objects.get(pk=id_to_delete)
        elif type == 'ts':
            obj = TrunkSequenceMatchedRoute.objects.get(pk=id_to_delete)
        else:
            messages.error(request, _("Somethings goes wrong!"))
            return redirect(self.redirect_url)

        to_redirect = obj.outbound
        obj.delete()
        messages.success(request, _("Deleted!"))
        return redirect(to_redirect)


class OutboundDetailView(LoginRequiredMixin, BaseClassView):
    """
        Detail of an outbound route, this view allows to edit the details of the selected outbound route
        and manage dial patterns and trunk sequence
    """

    template_name = 'xbee_routes/outbound_detail.html'

    def post(self, request, id_outbound):
        ob = get_object_or_404(Outbound, pk=id_outbound)
        if request.method == 'POST':
            if 'outbound_submitted' in request.POST:
                form = OutboundForm(request.POST, instance=ob)
                if form.is_valid():
                    form.save()
                    messages.success(request, _(f"Outbound route updated!"))

            if 'fs_dp_submitted' in request.POST:
                formset = DialPatternFormset(request.POST, prefix='dialpat')
                if formset.is_valid():
                    for form in formset:
                        if form.is_valid():
                            dp = form.save(commit=False)
                            dp.outbound = ob
                            dp.save()
                            messages.success(request, _(f"Dial Pattern created: {dp.prepend} - {dp.prefix}"))

            if 'fs_ts_submitted' in request.POST:
                formset = TrunkSequenceMatchedRouteFormset(request.POST, prefix='trunkseq')
                if formset.is_valid():
                    for form in formset:
                        if form.is_valid():
                            ts = form.save(commit=False)
                            ts.outbound = ob
                            ts.save()
                            messages.success(request, _(f"Trunk sequence created: {ts.trunk.name}"))

        return redirect(ob)

    def get(self, request, id_outbound):
        ob = get_object_or_404(Outbound, pk=id_outbound)
        comps = Company.objects.filter(pk=ob.company.pk)
        trunks = Trunk.objects.filter(company__in=comps)
        form_outbound = OutboundForm(instance=ob, initial={'companies': comps})
        dp_formset = DialPatternFormset(prefix='dialpat')
        ts_formset = TrunkSequenceMatchedRouteFormset(prefix='trunkseq', form_kwargs={'initial': {'trunk': trunks}})

        return render(request, self.template_name, {
            'outbound': ob,
            'form_outbound': form_outbound,
            'dp_formset': dp_formset,
            'ts_formset': ts_formset,
        })
