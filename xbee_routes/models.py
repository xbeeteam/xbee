from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from xbee_modules.models import PluggableModuleModel


class Inbound(PluggableModuleModel):
    id = models.AutoField(primary_key=True, db_column='inb_id')
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name='inbounds', db_column='inb_comp_id')
    did_number = models.CharField(default=None, null=True, blank=True, max_length=50, db_column='inb_did')
    prepend_cid = models.CharField(default=None, null=True, blank=True, max_length=50, db_column='inb_prepend_cid')
    description = models.TextField(db_column='inb_description')

    LANG_CHOICES = (
        ('it', _('Italian')),
        ('fr', _('French')),
        ('es', _('Spanish')),
        ('en', _('English')),
        ('ru', _('Russian')),
    )
    language = models.CharField(default='it', choices=LANG_CHOICES, max_length=2, db_column='inb_lang')

    CALL_REC_CHOICES = (
        ('Y', _('Yes')),
        ('N', _('No')),
        ('OD', _('On demand')),
    )
    call_recording = models.CharField(default='N', choices=CALL_REC_CHOICES, max_length=2,
                                      db_column='inb_call_recording')
    music_on_hold = models.ForeignKey('xbee_recordings.MusicOnHoldingCategory', null=False, on_delete=models.PROTECT,
                                      db_column='inb_music_on_hold')

    def __str__(self):
        return f"{self.company.name} [{self.did_number=}]"

    class Meta:
        verbose_name = 'Inbound'
        verbose_name_plural = 'Inbounds'
        db_table = 'xbee_inbound'


class Outbound(models.Model):
    id = models.AutoField(primary_key=True, db_column='outb_id')
    company = models.ForeignKey('xbee_company.Company', null=False, on_delete=models.CASCADE,
                                related_name='outbounds', db_column='outb_comp_id')
    route_name = models.CharField(max_length=32, db_column="outb_route_name")
    route_cid = models.CharField(max_length=32, db_column="outb_route_cid")

    YES_NO_CHOICES = (
        ('Y', _('Yes')),
        ('N', _('No')),
    )
    override_extension = models.CharField(default='N', max_length=2, choices=YES_NO_CHOICES,
                                          db_column='outb_override_extension')
    route_password = models.CharField(max_length=64, db_column="outb_route_password")
    music_on_hold = models.ForeignKey('xbee_recordings.MusicOnHoldingCategory', null=False, related_name="+",
                                      on_delete=models.CASCADE, db_column='outb_music_on_hold')
    call_recording = models.CharField(default='N', choices=YES_NO_CHOICES, max_length=2,
                                      db_column='outb_call_recording')

    def get_absolute_url(self):
        return reverse("xbee_routes:outbound_detail", kwargs={"id_outbound": self.pk})

    def __str__(self):
        return f"{self.company.name} [{self.route_name=}]"

    class Meta:
        verbose_name = 'Outbound'
        verbose_name_plural = 'Outbounds'
        db_table = 'xbee_outbound'


class TrunkSequenceMatchedRoute(models.Model):
    outbound = models.ForeignKey(Outbound, null=False, on_delete=models.PROTECT, related_name="trunk_sequences",
                                 db_column="tsmr_outbound")
    trunk = models.ForeignKey('xbee_trunk.Trunk', null=False, on_delete=models.PROTECT, related_name="trunk_sequences",
                              db_column="tsmr_trunk")

    def __str__(self):
        return f"{self.outbound.route_name} [{self.trunk.name}]"

    class Meta:
        verbose_name = 'TrunkSequenceMatchedRoute'
        verbose_name_plural = 'TrunkSequenceMatchedRoutes'
        db_table = 'xbee_trunksequencematchedroute'


class DialPattern(models.Model):
    outbound = models.ForeignKey(Outbound, null=False, on_delete=models.CASCADE, related_name="dial_patterns",
                                 db_column="dp_outbound")
    prepend = models.CharField(max_length=32, db_column="dp_prepend")
    prefix = models.CharField(max_length=32, db_column="dp_prefix")
    match_pattern = models.CharField(max_length=32, db_column="dp_match_pattern")
    caller_id = models.CharField(max_length=32, db_column="dp_caller_id")

    def __str__(self):
        return f"{self.prepend} [{self.outbound.route_name}]"

    class Meta:
        verbose_name = 'DialPattern'
        verbose_name_plural = 'DialPatterns'
        db_table = 'xbee_dialpattern'
