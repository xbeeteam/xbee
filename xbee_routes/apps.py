import sys
from django.apps import AppConfig

from xbee_routes.widgets import OutboundRoutesWidget


class XbeeRoutesConfig(AppConfig):
    name = 'xbee_routes'
    urls = 'routes'
    verbose_name = 'Routes'
    context = 'routes'
    widgets = [OutboundRoutesWidget, ]

    def ready(self):
        if 'migrate' not in sys.argv and 'makemigrations' not in sys.argv:
            from xbee_modules.models import Module

            try:
                Module.objects.get(name='Inbound routes')
            except Module.DoesNotExist:
                from django.contrib.contenttypes.models import ContentType
                from xbee_routes.models import Inbound, Outbound

                Module.objects.create(
                    name='Inbound routes',
                    context=f'{self.context}-in',
                    model_object=ContentType.objects.get_for_model(Inbound)
                ).save()

                Module.objects.create(
                    name='Outbound routes',
                    context=f'{self.context}-out',
                    model_object=ContentType.objects.get_for_model(Outbound)
                ).save()
