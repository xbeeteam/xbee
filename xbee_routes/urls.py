from django.urls import path

from xbee_routes import views

app_name = 'xbee_routes'

urlpatterns = [
    path('', views.RoutesView.as_view(), name='routes'),
    path('delete/<str:type>/<int:id_route>', views.DeleteRouteView.as_view(), name='delete_route'),
    path('<str:type>/delete/<int:id_to_delete>', views.DeleteDialPatternTrunkSequenceView.as_view(),
         name='delete_dp_and_ts'),
    path('outbound/detail/<int:id_outbound>', views.OutboundDetailView.as_view(), name='outbound_detail'),

]
