from django.forms import ModelForm, formset_factory

from xbee_modules.forms import PluggableModuleModelForm
from xbee_routes.models import Inbound, Outbound, DialPattern, TrunkSequenceMatchedRoute


class InboundForm(PluggableModuleModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if categs := initial.get('music_on_hold_cats', False):
                self.fields['music_on_hold'].queryset = categs

    class Meta:
        model = Inbound
        fields = '__all__'
        exclude = PluggableModuleModelForm.Meta.exclude


class OutboundForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if comps := initial.get('companies', False):
                self.fields['company'].queryset = comps
            if categs := initial.get('music_on_hold_cats', False):
                self.fields['music_on_hold'].queryset = categs

    class Meta:
        model = Outbound
        fields = '__all__'


class DialPatternForm(ModelForm):
    class Meta:
        model = DialPattern
        fields = '__all__'
        exclude = ('outbound', )


class TrunkSequenceMatchedRouteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if initial := kwargs.get('initial'):
            if trunk := initial.get('trunk', False):
                self.fields['trunk'].queryset = trunk

    class Meta:
        model = TrunkSequenceMatchedRoute
        fields = '__all__'
        exclude = ('outbound', )


DialPatternFormset = formset_factory(DialPatternForm)
TrunkSequenceMatchedRouteFormset = formset_factory(TrunkSequenceMatchedRouteForm)
