from django.contrib import admin

from xbee_routes.models import Inbound, Outbound, DialPattern, TrunkSequenceMatchedRoute

admin.site.register(Inbound)
admin.site.register(Outbound)
admin.site.register(TrunkSequenceMatchedRoute)
admin.site.register(DialPattern)
